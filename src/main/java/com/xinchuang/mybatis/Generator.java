package com.xinchuang.mybatis;


import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * <p>
 * mysql 代码生成器
 * </p>
 */
public class Generator {
    /**
     * RUN THIS
     */
    public static void main(String[] args) {
        //获取控制台的数据
        Scanner scanner = new Scanner(System.in);
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();
        // 全局配置
        GlobalConfig gc = new GlobalConfig();
//        System.out.println("请输入文件输出目录的模块或者项目的地址:");
//        String projectPath = scanner.nextLine();
        String projectPath = "D:\\desktop\\项目\\昆明\\apiBuild";
        //生成文件的输出目录
        gc.setOutputDir(projectPath + "/src/main/java");
        //作者
        gc.setAuthor("zx");
        //是否覆蓋已有文件 默认值：false
        gc.setFileOverride(true);
        //是否打开输出目录 默认值:true
        gc.setOpen(false);
        //开启 baseColumnList 默认false
        gc.setBaseColumnList(true);
        //开启 BaseResultMap 默认false
        gc.setBaseResultMap(true);
        //实体命名方式  默认值：null 例如：%sEntity 生成 UserEntity
//      gc.setEntityName("%sEntity");
        //mapper 命名方式 默认值：null 例如：%sDao 生成 UserDao
        gc.setMapperName("%sMapper");
        //Mapper xml 命名方式   默认值：null 例如：%sDao 生成 UserDao.xml
        gc.setXmlName("%sMapper");
        //service 命名方式   默认值：null 例如：%sBusiness 生成 UserBusiness
        gc.setServiceName("%sService");
        //service impl 命名方式  默认值：null 例如：%sBusinessImpl 生成 UserBusinessImpl
        gc.setServiceImplName("%sServiceImpl");
        //controller 命名方式    默认值：null 例如：%sAction 生成 UserAction
        gc.setControllerName("%sController");
        mpg.setGlobalConfig(gc);
        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl("jdbc:mysql://117.176.220.75:10126/target_system?useUnicode=true&useSSL=false&characterEncoding=utf8");
        // dsc.setSchemaName("public");
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setUsername("target_system_root");
        dsc.setPassword("Target#1234");
        mpg.setDataSource(dsc);
        // 包配置
        PackageConfig pc = new PackageConfig();
        //      pc.setModuleName(scanner("模块名"));
        //      pc.setParent("com.stu");
//        System.out.println("请输入模块名:");
//        String name = scanner.nextLine();
        //自定义包配置
        pc.setParent("com.xinchuang");
        pc.setModuleName(null);
        pc.setMapper("mapper.common");
        pc.setEntity("entity.common");
        pc.setService("service.common");
        pc.setServiceImpl("service.common.impl");
        pc.setController("controller.common");
        mpg.setPackageInfo(pc);
        // 自定义配置
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
        // to do nothing
            }
        };
        List<FileOutConfig> focList = new ArrayList<>();
        focList.add(new FileOutConfig("/templates/mapper.xml.ftl") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输入文件名称
                return projectPath + "/src/main/resources/mapper/" + /*pc.getModuleName() + "/" +*/
                        tableInfo.getEntityName() + "Mapper" +
                        StringPool.DOT_XML;
            }
        });
        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);
        mpg.setTemplate(new TemplateConfig().setXml(null));
        // 策略配置	数据库表配置，通过该配置，可指定需要生成哪些表或者排除哪些表
        StrategyConfig strategy = new StrategyConfig();
        //表名生成策略
        strategy.setNaming(NamingStrategy.underline_to_camel);
        //数据库表字段映射到实体的命名策略, 未指定按照 naming 执行
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        // 全局大写命名 ORACLE 注意
//	    strategy.setCapitalMode(true);
        //表前缀
//	    strategy.setTablePrefix("prefix");
        //自定义继承的Entity类全称，带包名
//	    strategy.setSuperEntityClass("com.stu.domain");
        //自定义实体，公共字段
//	    strategy.setSuperEntityColumns(new String[] { "test_id", "age" });
        //【实体】是否为lombok模型（默认 false)
        strategy.setEntityLombokModel(true);
        //生成 @RestController 控制器
        strategy.setRestControllerStyle(true);
        //自定义继承的Controller类全称，带包名
//	    strategy.setSuperControllerClass("com.baomidou.ant.common.BaseController");
        //需要包含的表名，允许正则表达式（与exclude二选一配置）
//      strategy.setInclude(scanner("表名"));
        System.out.println("请输入映射的表名:");
        String tables = scanner.nextLine();
        String[] num = tables.split(",");
        // 需要生成的表可以多张表
        strategy.setInclude(num);
        // 排除生成的表
//	    strategy.setExclude(new String[]{"test"});
        //如果数据库有前缀，生成文件时是否要前缀acl_
//      strategy.setTablePrefix("bus_");
//      strategy.setTablePrefix("sys_");
        //驼峰转连字符
        strategy.setControllerMappingHyphenStyle(true);
        //是否生成实体时，生成字段注解
        strategy.setTablePrefix(pc.getModuleName() + "_");
        mpg.setStrategy(strategy);
        mpg.setTemplateEngine(new FreemarkerTemplateEngine());
        mpg.execute();
    }

}
