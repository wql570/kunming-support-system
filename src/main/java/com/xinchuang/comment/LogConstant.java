package com.xinchuang.comment;

/**
 * @title : 常量类
 * @description :  = 无)
 * <p>
 * Created by Jjddppz On 2020/1/18 11:23
 */
public interface LogConstant {
    Integer TOP_PARENT_ID = 0;
    String DEFAULT_NULL_MESSAGE = "暂无承载数据";
    String DEFAULT_SUCCESS_MESSAGE = "操作成功";
    String DEFAULT_FAILURE_MESSAGE = "操作失败";
    String EMPTY = "清空";
    String ADD = "存储";
    String DEL = "删除";
    String UPDATE = "修改";
    String SELECT = "查询";
    String IMMIGRATION = "迁入";
    String EMIGRATION = "迁出";
    String MODULE_ACHIEVEMENT = "成果数据服务";
    String MODULE_BUSINESS = "业务数据服务";
    String MODULE_ORIGINAL = "原始数据服务";
    String MODULE_STANDARD = "标准化数据服务";
    String MODULE_SYSTEM = "系统服务";
    String MODULE_ELEMENT_CHANGE = "要素审核服务";
    String MODULE_TARGET_CHANGE = "动向审核服务";

}