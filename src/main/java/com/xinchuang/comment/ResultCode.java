package com.xinchuang.comment;

import com.xinchuang.comment.IResultCode;

/**
 * @title : 返回结果基本信息枚举
 * @description : (无)
 *
 * @author : Jjddppz
 * @createDate : 2020/2/11
 * @version : 1.0
 **/
public enum ResultCode implements IResultCode {
    NUKOWN(100, "未知的错误"),
    SUCCESS(200, "操作成功"),
    FAILURE(400, "业务异常"),

    /**
    * 请求错误
    */
    CLIENT_UN_AUTHORIZED(401, "客户端请求未授权"),
    UN_AUTHORIZED(401, "请求未授权"),
    REQ_REJECT (403, "请求被拒绝"),
    NOT_FOUND (404, "404 没找到请求"),
    SYS_ERROR(404, "调用系统接口失败"),
    METHOD_NOT_SUPPORTED (405, "不支持当前请求方法"),
    MEDIA_TYPE_NOT_SUPPORTED (415, "不支持当前媒体类型"),
    PARAM_VALID_ERROR (4000, "参数校验失败"),
    PARAM_TYPE_ERROR (4001, "请求参数类型错误"),
    PARAM_MISS (4002, "缺少必要的请求参数"),
    PARAM_BIND_ERROR(40003, "请求参数绑定错误"),
    MSG_NOT_READABLE(40004, "消息不能读取"),

    /**
    * 服务器错误
    */
    SERVER_ERROR (500, "服务器错误"),
    SERVER_NOT_ALLOT (501, "服务权限未分配"),
    SERVER_NOT_CLOSE (502, "服务已经关闭"),

    /**
    * TOKEN错误
    */
    INVALID_APP_ID(601, "App ID无效"),
    TOKEN_EXPIRED (602, "Token 已过期"),
    INVALID_TOKEN (603, "Token无效"),
    UNAUTHORIZED (604, "请求未授权"),
    ERROR_PERMISSION (605, "没有权限"),

    /**
    * 社区错误
    */
    UPLIMIT (701, "已达上限"),
    REPEAT_ATTENTION (702, "重复关注"),
    SCORE_EXISTING (703, "评分已存在"),
    REPEAT_PRAISE (704, "重复点赞"),

    /**
    * 图片错误
    */
    IMAGE_ILLEGAL(801, "非法图片"),
    IMAGE_FORMAT_ERROR (802, "暂不支持当前图片格式"),
    IMAGE_UPLOAD_ERROR (803, "图片上传失败,请重试"),
    FILE_TOO_MUCH (804, "文件太大"),

    /**
    * 登录注册错误
    */
    CODE_ERROR (10001, "验证码错误"),
    USER_EMPTY (10002, "用户不存在,请联系管理员处理"),
    PASSWORD_ERROR (10003, "账号或密码错误,请重新输入"),
    USERNAME_PASSWORD_ERROR (10004, "用户名或密码不能为空"),
    USER_EXIST (10005, "用户名重复,请修改后重试"),
    PHONE_EXIST (10006, "手机号已存在"),
    PHONE_NOT_EXISTS (10007, "手机号不存在"),
    ACCOUNT_DISABLE (10008, "账号被禁用"),
    ROLE_EXISTS (10009, "角色已存在"),
    ROLE_NOT_EXISTS (10010, "角色不存在"),
    NOT_REAL_NAME_SYSTEM (10011, "未实名"),
    FACE_ERROR (10012, "图片非人像,请重新上传!"),

    /**
    * 其他错误
    */
    STATUS_EMPTY (20002, "状态不存在,请稍后重试"),
    DELETE_EMPTY(20003, "删除的信息不能为空"),
    ADD_EMPTY (20004, "新增的信息不能为空"),
    COMMON_KEY_EXIST (20005,"字段名称已重复,请修改后重试"),
    COMMON_KEY_NAME_EXIST (20006, "属性名称已重复,请修改后重试"),
    COMMON_EMPTY (20007, "当前属性已删除/不存在,请勿重复操作"),
    UPDATE_EMPTY (20008,"更新的信息不能为空"),
    SIGN_ERROR (20009, "新增格式不正确,请联系管理员处理"),
    PARENT_ERROR (20010,"无效的子节点,请联系管理员处理"),
    TARGET_TYPE_NAME_EXIST (20011, "节点名称已存在,请修改后重试"),
    TARGET_TYPE_PARENT_NOT_DEL(20012, "根节点不能删除,重新选择"),
    TARGET_TYPE_PARENT_UPDATE (20013, "根节点不能修改,重新选择"),
    TYPE_ID_EMPTY (20014, "请选择需要查看的类别"),
    TYPE_EMPTY (20015, "类别不存在/已被删除,重新选择"),
    TYPE_UPLOAD_EMPTY (20016, "请选择需要上传的类别"),
    SQL_ERROR (20017, "数据写入失败,请稍后重试"),
    DELETE_EMPTY_ERROR (20018,"当先信息已被删除/不存在"),
    ROOT_ERROR (20019, "不能选择根节点作为类别,请重新选择"),
    ADD_TARGET_DATA_LIST_ERROR (20020, "修改的信息不能为空"),
    SELECT_EMPTY (20021, "查询信息不存在/已删除");

    final int code;
    final String message;

    @Override
    public int getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    private ResultCode(final int code, final String message) {
        this.code = code;
        this.message = message;
    }

    public static String getMessage(int code) {
        for (ResultCode res : ResultCode.values()) {
            if (code == res.getCode()) {
                return res.getMessage();
            }
        }
        return NUKOWN.getMessage();
    }

}