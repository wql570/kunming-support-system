package com.xinchuang.controller;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.xinchuang.utils.CheckUtil;
import com.xinchuang.comment.R;
import com.xinchuang.comment.ResultCode;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;

import com.xinchuang.utils.DateUtils;
import org.springframework.web.multipart.MultipartFile;
import org.apache.commons.lang3.StringUtils;
import com.xinchuang.service.DictionariesService;

/**
 * DictionariesController
 *
 * @date 2023-08-25
 */
@RestController
@RequestMapping("/dictionaries" )
@Api(value = "字典维护", tags = "字典维护")
public class DictionariesController {
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss" );

    @Resource
    private DictionariesService dictionariesService;

        /**
     * 属性列表接口  函数名称attributeList  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/attributeList" )
            @ApiOperation("属性列表")
                                    public R attributeList(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + attributeListACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = dictionariesService.attributeList(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + attributeListACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 属性编辑接口  函数名称attributeEdit  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/attributeEdit" )
            @ApiOperation("属性编辑")
                                    public R attributeEdit(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + attributeEditACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = dictionariesService.attributeEdit(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + attributeEditACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 删除属性接口  函数名称attributeDelete  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/attributeDelete" )
            @ApiOperation("删除属性")
                                    public R attributeDelete(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + attributeDeleteACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = dictionariesService.attributeDelete(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + attributeDeleteACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 查询类型列表接口  函数名称typeList  功能描述:
                     * @return 返回参数{}
     */
            @PostMapping("/typeList" )
            @ApiOperation("查询类型列表")
                                    public R typeList() {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    String result = "";
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = dictionariesService.typeList();
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + typeListACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 编辑类型接口  函数名称typeEdit  功能描述:新增的时候默认新增下级
                     * @return 返回参数{}
     */
            @PostMapping("/typeEdit" )
            @ApiOperation("编辑类型")
                                    public R typeEdit() {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    String result = "";
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = dictionariesService.typeEdit();
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + typeEditACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 删除类型接口  函数名称typeDelete  功能描述:
                     * @return 返回参数{}
     */
            @PostMapping("/typeDelete" )
            @ApiOperation("删除类型")
                                    public R typeDelete() {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    String result = "";
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = dictionariesService.typeDelete();
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + typeDeleteACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 类型属性列表接口  函数名称typeAttributeList  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/typeAttributeList" )
            @ApiOperation("类型属性列表")
                                    public R typeAttributeList(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + typeAttributeListACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = dictionariesService.typeAttributeList(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + typeAttributeListACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 类型属性编辑接口  函数名称typeAttributeEdit  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/typeAttributeEdit" )
            @ApiOperation("类型属性编辑")
                                    public R typeAttributeEdit(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + typeAttributeEditACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = dictionariesService.typeAttributeEdit(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + typeAttributeEditACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 属性维护删除接口  函数名称typeAttributeDelete  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/typeAttributeDelete" )
            @ApiOperation("属性维护删除")
                                    public R typeAttributeDelete(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + typeAttributeDeleteACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = dictionariesService.typeAttributeDelete(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + typeAttributeDeleteACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 知识种类查询接口  函数名称knowList  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/knowList" )
            @ApiOperation("知识种类查询")
                                    public R knowList(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + knowListACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = dictionariesService.knowList(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + knowListACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 知识种类编辑接口  函数名称knowEdit  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/knowEdit" )
            @ApiOperation("知识种类编辑")
                                    public R knowEdit(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + knowEditACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = dictionariesService.knowEdit(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + knowEditACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 知识种类删除接口  函数名称knowDelete  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/knowDelete" )
            @ApiOperation("知识种类删除")
                                    public R knowDelete(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + knowDeleteACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = dictionariesService.knowDelete(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + knowDeleteACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 上移下移特有接口接口  函数名称upDownSpec  功能描述:sort:0123--上移，上移置顶，下移，下移置顶
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/upDownSpec" )
            @ApiOperation("上移下移特有接口")
                                    public R upDownSpec(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + upDownSpecACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = dictionariesService.upDownSpec(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + upDownSpecACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 上移下移接口  函数名称upDown  功能描述:type:0MB,1子MB；
sort:0123--上移，上移置顶，下移，下移置顶
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/upDown" )
            @ApiOperation("上移下移")
                                    public R upDown(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + upDownACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = dictionariesService.upDown(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + upDownACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

    
    public static String jsonLoop(Object object, String result, String methodName, Set<String> set) {
        Object v = null;
        if (object instanceof JSONArray || object instanceof ArrayList) {
            JSONArray jsonArray = new JSONArray();
            if (object instanceof ArrayList){
                jsonArray = JSONArray.parseArray(JSON.toJSONString(object));
            }else {
                jsonArray = (JSONArray) object;
            }
            for (int i = 0; i < jsonArray.size(); i++) {
                result = jsonLoop(jsonArray.get(i), result, methodName, set);
                if (StringUtils.isNotEmpty(result)) {
                    return result;
                }
            }
        }
        if (object instanceof JSONObject) {
            JSONObject jsonObject = (JSONObject) object;
            for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
                Object o = entry.getValue();
                if (o instanceof JSONArray || o instanceof ArrayList) {
                    result += selectCheckMathod(entry.getKey(), entry.getValue(), methodName, set);
                    result = jsonLoop(o, result, methodName, set);
                    if (StringUtils.isNotEmpty(result)) {
                        return result;
                    }
                } else if (o instanceof JSONObject) {
                    result = jsonLoop(o, result, methodName, set);
                    if (StringUtils.isNotEmpty(result)) {
                        return result;
                    }
                } else {
//                   FIXME: 未知类型
                }
                v = entry.getValue();
                result += selectCheckMathod(entry.getKey(), v, methodName, set);
            }
        }
        return result;
    }

    private static String selectCheckMathod(String key, Object value, String methodName, Set<String> set) {
        String result = "";
                            if ("attributeListSC".equals(methodName)) {
                result = attributeListSC(key, value, set);
            }
                    if ("attributeListAC".equals(methodName)) {
                result = attributeListAC(key, value, set);
            }
                            if ("attributeEditSC".equals(methodName)) {
                result = attributeEditSC(key, value, set);
            }
                    if ("attributeEditAC".equals(methodName)) {
                result = attributeEditAC(key, value, set);
            }
                            if ("attributeDeleteSC".equals(methodName)) {
                result = attributeDeleteSC(key, value, set);
            }
                    if ("attributeDeleteAC".equals(methodName)) {
                result = attributeDeleteAC(key, value, set);
            }
                            if ("typeListSC".equals(methodName)) {
                result = typeListSC(key, value, set);
            }
                    if ("typeListAC".equals(methodName)) {
                result = typeListAC(key, value, set);
            }
                            if ("typeEditSC".equals(methodName)) {
                result = typeEditSC(key, value, set);
            }
                    if ("typeEditAC".equals(methodName)) {
                result = typeEditAC(key, value, set);
            }
                            if ("typeDeleteSC".equals(methodName)) {
                result = typeDeleteSC(key, value, set);
            }
                    if ("typeDeleteAC".equals(methodName)) {
                result = typeDeleteAC(key, value, set);
            }
                            if ("typeAttributeListSC".equals(methodName)) {
                result = typeAttributeListSC(key, value, set);
            }
                    if ("typeAttributeListAC".equals(methodName)) {
                result = typeAttributeListAC(key, value, set);
            }
                            if ("typeAttributeEditSC".equals(methodName)) {
                result = typeAttributeEditSC(key, value, set);
            }
                    if ("typeAttributeEditAC".equals(methodName)) {
                result = typeAttributeEditAC(key, value, set);
            }
                            if ("typeAttributeDeleteSC".equals(methodName)) {
                result = typeAttributeDeleteSC(key, value, set);
            }
                    if ("typeAttributeDeleteAC".equals(methodName)) {
                result = typeAttributeDeleteAC(key, value, set);
            }
                            if ("knowListSC".equals(methodName)) {
                result = knowListSC(key, value, set);
            }
                    if ("knowListAC".equals(methodName)) {
                result = knowListAC(key, value, set);
            }
                            if ("knowEditSC".equals(methodName)) {
                result = knowEditSC(key, value, set);
            }
                    if ("knowEditAC".equals(methodName)) {
                result = knowEditAC(key, value, set);
            }
                            if ("knowDeleteSC".equals(methodName)) {
                result = knowDeleteSC(key, value, set);
            }
                    if ("knowDeleteAC".equals(methodName)) {
                result = knowDeleteAC(key, value, set);
            }
                            if ("upDownSpecSC".equals(methodName)) {
                result = upDownSpecSC(key, value, set);
            }
                    if ("upDownSpecAC".equals(methodName)) {
                result = upDownSpecAC(key, value, set);
            }
                            if ("upDownSC".equals(methodName)) {
                result = upDownSC(key, value, set);
            }
                    if ("upDownAC".equals(methodName)) {
                result = upDownAC(key, value, set);
            }
                return result;
    }


                private static String attributeListSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("pageNum".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "页码" , isNull, "0", "99999");
                                                                                                                                                                                                }
                                        if ("pangeSize".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "页容" , isNull, "0", "99999");
                                                                                                                                                                                                }
                                        if ("type".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String attributeListAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                                        if ("pageNum".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "页码", isNull, "0", "99999");
                                                                                                                                                                                                }
                                        if ("pangeSize".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "页容", isNull, "0", "99999");
                                                                                                                                                                                                }
                                        if ("pageAll".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "页总", isNull, "0", "99999");
                                                                                                                                                                                                }
                                        if ("total".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "总条数", isNull, "0", "99999");
                                                                                                                                                                                                }
                                        if ("attributeList".equals(key)) {
                                                                                                                        result += CheckUtil.checkJSONArray(JSONArray.parseArray(JSON.toJSONString(value)), "属性列表", isNull);
                                    }
                                        if ("id".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("keyName".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "名称", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("key".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "字段", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("keyType".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "类型", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("showType".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "显示类型", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("row".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "文本域行数", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("required".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "必填", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("message".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "提示信息", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("description".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "模板信息", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("minmax".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "最大最小值", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("unit".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "单位", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("statis".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "是否统计", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("enumerator".equals(key)) {
                                                                                                                        result += CheckUtil.checkJSONArray(JSONArray.parseArray(JSON.toJSONString(value)), "枚举值列表", isNull);
                                    }
                                        if ("label".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "名称", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("value".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "值", isNull, 0, 255);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String attributeEditSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("type".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                                        if ("id".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("keyName".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "名称" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("key".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "字段" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("keyType".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "类型" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("showType".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "显示类型" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("row".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "文本域行数" , isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("required".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "必填" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("message".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "提示信息" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("description".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "模板信息" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("minmax".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "最大最小值" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("unit".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "单位" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("statis".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "是否统计" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("label".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "名称" , isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("value".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "值" , isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("enumerator".equals(key)) {
                                                                                                                        result += CheckUtil.checkJSONArray(JSONArray.parseArray(JSON.toJSONString(value)),"枚举值列表", isNull);
                                    }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String attributeEditAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String attributeDeleteSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("type".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                                        if ("id".equals(key)) {
                                                                                                            isNull = true;
                                                // 判断参数类型: String  num
                                                    result += CheckUtil.checkJSONArray(JSONArray.parseArray(JSON.toJSONString(value)), "标识" , isNull, 1000, 2, 1, 0, 50);
                        
                                                                            }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String attributeDeleteAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String typeListSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String typeListAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String typeEditSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String typeEditAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String typeDeleteSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String typeDeleteAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String typeAttributeListSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("pangeSize".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "页容" , isNull, "0", "99999");
                                                                                                                                                                                                }
                                        if ("pageNum".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "页码" , isNull, "0", "99999");
                                                                                                                                                                                                }
                                        if ("typeId".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String typeAttributeListAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("pageNum".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "页码", isNull, "0", "99999");
                                                                                                                                                                                                }
                                        if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                                        if ("pangeSize".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "页容", isNull, "0", "99999");
                                                                                                                                                                                                }
                                        if ("pageAll".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "页总", isNull, "0", "99999");
                                                                                                                                                                                                }
                                        if ("total".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "总条数", isNull, "0", "99999");
                                                                                                                                                                                                }
                                        if ("attributeList".equals(key)) {
                                                                                                                        result += CheckUtil.checkJSONArray(JSONArray.parseArray(JSON.toJSONString(value)), "属性列表", isNull);
                                    }
                                        if ("id".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("keyName".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "名称", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("key".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "字段", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("keyType".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "类型", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("showType".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "显示类型", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("row".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "文本域行数", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("required".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "必填", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("message".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "提示信息", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("description".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "模板信息", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("minmax".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "最大最小值", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("unit".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "单位", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("statis".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "是否统计", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("value".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "值", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("label".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "名称", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("enumerator".equals(key)) {
                                                                                                                        result += CheckUtil.checkJSONArray(JSONArray.parseArray(JSON.toJSONString(value)), "枚举值列表", isNull);
                                    }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String typeAttributeEditSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("key".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "字段" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("keyName".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "名称" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("typeId".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("keyType".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "类型" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("showType".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "显示类型" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("row".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "文本域行数" , isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("required".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "必填" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("message".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "提示信息" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("description".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "模板信息" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("minmax".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "最大最小值" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("unit".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "单位" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("statis".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "是否统计" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("enumerator".equals(key)) {
                                                                                                                        result += CheckUtil.checkJSONArray(JSONArray.parseArray(JSON.toJSONString(value)),"枚举值列表", isNull);
                                    }
                                        if ("value".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "值" , isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("label".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "名称" , isNull, 0, 255);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String typeAttributeEditAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String typeAttributeDeleteSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("typeId".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String typeAttributeDeleteAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String knowListSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("pageNum".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "页码" , isNull, "0", "99999");
                                                                                                                                                                                                }
                                        if ("pageSize".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "页容" , isNull, "0", "99999");
                                                                                                                                                                                                }
                                        if ("typeId".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String knowListAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                                        if ("pageNum".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "页码", isNull, "0", "99999");
                                                                                                                                                                                                }
                                        if ("pangeSize".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "页容", isNull, "0", "99999");
                                                                                                                                                                                                }
                                        if ("pageAll".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "页总", isNull, "0", "99999");
                                                                                                                                                                                                }
                                        if ("total".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "总条数", isNull, "0", "99999");
                                                                                                                                                                                                }
                                        if ("dataList".equals(key)) {
                                                                                                                        result += CheckUtil.checkJSONArray(JSONArray.parseArray(JSON.toJSONString(value)), "知识列表", isNull);
                                    }
                                        if ("id".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("kindName".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "知识种类", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("knowName".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "知识名称", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("knowType".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "知识类型", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("index".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "指标选择", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("describe".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "知识描述", isNull, 0, 255);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String knowEditSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("describe".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "知识描述" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("type".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "类型" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("typeId".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "类型标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("index".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "指标选择" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("id".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("kindName".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "知识种类" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("knowName".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "知识名称" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("knowType".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "知识类型" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String knowEditAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String knowDeleteSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("typeId".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "类型标识" , isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("id".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String knowDeleteAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String upDownSpecSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("typeId".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("sort".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "移动" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String upDownSpecAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String upDownSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("sort".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "移动" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("id".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("type".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "类别" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String upDownAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
    
                private static String attributeListACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("type" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String attributeListACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String attributeEditACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("type" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String attributeEditACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String attributeDeleteACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("id" );
                            list.add("type" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String attributeDeleteACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String typeListACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String typeListACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String typeEditACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String typeEditACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String typeDeleteACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String typeDeleteACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String typeAttributeListACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("typeId" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String typeAttributeListACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String typeAttributeEditACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("typeId" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String typeAttributeEditACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String typeAttributeDeleteACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("typeId" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String typeAttributeDeleteACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String knowListACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("pageSize" );
                            list.add("typeId" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String knowListACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String knowEditACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("typeId" );
                            list.add("id" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String knowEditACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String knowDeleteACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("id" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String knowDeleteACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String upDownSpecACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("typeId" );
                            list.add("sort" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String upDownSpecACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String upDownACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("sort" );
                            list.add("id" );
                            list.add("type" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String upDownACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
    
    /**
     * 获取集合中的不同值
     *
     * @param list
     * @param set
     * @return
     */
    private static String getCollectionDifferentValue(List<String> list, Set<String> set, String flag){
        String result = "";
        for (String str : list){
            if (!set.contains(str)){
                result += str + ",";
            }
        }
        if (StringUtils.isNotEmpty(result)){
            result = StringUtils.strip(result, ",");
            result = "---" + flag + ":" + result + "为必填参数";
        }
        return result;
    }
}
