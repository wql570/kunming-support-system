package com.xinchuang.controller.target;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@RestController
@RequestMapping("/target-main-table")
public class TargetMainTableController {

}
