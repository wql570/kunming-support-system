package com.xinchuang.controller.image;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@RestController
@RequestMapping("/image-editing")
public class ImageEditingController {

}
