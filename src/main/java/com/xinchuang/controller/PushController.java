package com.xinchuang.controller;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.xinchuang.utils.CheckUtil;
import com.xinchuang.comment.R;
import com.xinchuang.comment.ResultCode;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;

import com.xinchuang.utils.DateUtils;
import org.springframework.web.multipart.MultipartFile;
import org.apache.commons.lang3.StringUtils;
import com.xinchuang.service.PushService;

/**
 * PushController
 *
 * @date 2023-08-25
 */
@RestController
@RequestMapping("/push" )
@Api(value = "目指推送", tags = "目指推送")
public class PushController {
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss" );

    @Resource
    private PushService pushService;

        /**
     * 简报列表接口  函数名称reportList  功能描述:type:0简报1精度2整编
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/reportList" )
            @ApiOperation("简报列表")
                                    public R reportList(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + reportListACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = pushService.reportList(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + reportListACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 服务器设置接口  函数名称serveEdit  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/serveEdit" )
            @ApiOperation("服务器设置")
                                    public R serveEdit(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + serveEditACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = pushService.serveEdit(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + serveEditACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 共享服务器接口  函数名称serveUpload  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/serveUpload" )
            @ApiOperation("共享服务器")
                                    public R serveUpload(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + serveUploadACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = pushService.serveUpload(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + serveUploadACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 上传简报接口  函数名称reportUpload  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/reportUpload" )
            @ApiOperation("上传简报")
                                    public R reportUpload(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + reportUploadACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = pushService.reportUpload(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + reportUploadACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 下载简报接口  函数名称reportDownload  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/reportDownload" )
            @ApiOperation("下载简报")
                                    public R reportDownload(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + reportDownloadACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = pushService.reportDownload(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + reportDownloadACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 删除简报接口  函数名称reportDelete  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/reportDelete" )
            @ApiOperation("删除简报")
                                    public R reportDelete(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + reportDeleteACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = pushService.reportDelete(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + reportDeleteACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

    
    public static String jsonLoop(Object object, String result, String methodName, Set<String> set) {
        Object v = null;
        if (object instanceof JSONArray || object instanceof ArrayList) {
            JSONArray jsonArray = new JSONArray();
            if (object instanceof ArrayList){
                jsonArray = JSONArray.parseArray(JSON.toJSONString(object));
            }else {
                jsonArray = (JSONArray) object;
            }
            for (int i = 0; i < jsonArray.size(); i++) {
                result = jsonLoop(jsonArray.get(i), result, methodName, set);
                if (StringUtils.isNotEmpty(result)) {
                    return result;
                }
            }
        }
        if (object instanceof JSONObject) {
            JSONObject jsonObject = (JSONObject) object;
            for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
                Object o = entry.getValue();
                if (o instanceof JSONArray || o instanceof ArrayList) {
                    result += selectCheckMathod(entry.getKey(), entry.getValue(), methodName, set);
                    result = jsonLoop(o, result, methodName, set);
                    if (StringUtils.isNotEmpty(result)) {
                        return result;
                    }
                } else if (o instanceof JSONObject) {
                    result = jsonLoop(o, result, methodName, set);
                    if (StringUtils.isNotEmpty(result)) {
                        return result;
                    }
                } else {
//                   FIXME: 未知类型
                }
                v = entry.getValue();
                result += selectCheckMathod(entry.getKey(), v, methodName, set);
            }
        }
        return result;
    }

    private static String selectCheckMathod(String key, Object value, String methodName, Set<String> set) {
        String result = "";
                            if ("reportListSC".equals(methodName)) {
                result = reportListSC(key, value, set);
            }
                    if ("reportListAC".equals(methodName)) {
                result = reportListAC(key, value, set);
            }
                            if ("serveEditSC".equals(methodName)) {
                result = serveEditSC(key, value, set);
            }
                    if ("serveEditAC".equals(methodName)) {
                result = serveEditAC(key, value, set);
            }
                            if ("serveUploadSC".equals(methodName)) {
                result = serveUploadSC(key, value, set);
            }
                    if ("serveUploadAC".equals(methodName)) {
                result = serveUploadAC(key, value, set);
            }
                            if ("reportUploadSC".equals(methodName)) {
                result = reportUploadSC(key, value, set);
            }
                    if ("reportUploadAC".equals(methodName)) {
                result = reportUploadAC(key, value, set);
            }
                            if ("reportDownloadSC".equals(methodName)) {
                result = reportDownloadSC(key, value, set);
            }
                    if ("reportDownloadAC".equals(methodName)) {
                result = reportDownloadAC(key, value, set);
            }
                            if ("reportDeleteSC".equals(methodName)) {
                result = reportDeleteSC(key, value, set);
            }
                    if ("reportDeleteAC".equals(methodName)) {
                result = reportDeleteAC(key, value, set);
            }
                return result;
    }


                private static String reportListSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("pageNum".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "页码" , isNull, "0", "99999");
                                                                                                                                                                                                }
                                        if ("pageSize".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "页容" , isNull, "0", "99999");
                                                                                                                                                                                                }
                                        if ("fileName".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "名称" , isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("type".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "类型" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String reportListAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                                        if ("pageNum".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "页码", isNull, "0", "99999");
                                                                                                                                                                                                }
                                        if ("pageSize".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "页容", isNull, "0", "99999");
                                                                                                                                                                                                }
                                        if ("pageAll".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "页总", isNull, "0", "99999");
                                                                                                                                                                                                }
                                        if ("total".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "总条数", isNull, "0", "99999");
                                                                                                                                                                                                }
                                        if ("fileList".equals(key)) {
                                                                                                                        result += CheckUtil.checkJSONArray(JSONArray.parseArray(JSON.toJSONString(value)), "列表", isNull);
                                    }
                                        if ("id".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("fileName".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "名称", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("userName".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "创建人", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("number".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "下载次数", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("updateTime".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "创建时间", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("fileUrl".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "文件", isNull, 0, 50);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String serveEditSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("ip".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "地址" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("port".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "端口" , isNull, "0", "65535");
                                                                                                                                                                                                }
                                        if ("remark".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "备注" , isNull, 0, 255);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String serveEditAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String serveUploadSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("ids".equals(key)) {
                                                                                                            isNull = true;
                                                // 判断参数类型: String  num
                                                    result += CheckUtil.checkJSONArray(JSONArray.parseArray(JSON.toJSONString(value)), "标识列表" , isNull, 1000, 2, 1, 0, 50);
                        
                                                                            }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String serveUploadAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String reportUploadSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("type".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "类型" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("fileName".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "名称" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("fileUrl".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "文件地址" , isNull, 0, 999);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String reportUploadAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String reportDownloadSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("type".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "类型" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("ids".equals(key)) {
                                                                                                            isNull = true;
                                                // 判断参数类型: String  num
                                                    result += CheckUtil.checkJSONArray(JSONArray.parseArray(JSON.toJSONString(value)), "标识列表" , isNull, 1000, 2, 1, 0, 50);
                        
                                                                            }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String reportDownloadAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                                        if ("fileUrl".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "地址", isNull, 0, 999);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String reportDeleteSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("type".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "类型" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("ids".equals(key)) {
                                                                                                            isNull = true;
                                                // 判断参数类型: String  num
                                                    result += CheckUtil.checkJSONArray(JSONArray.parseArray(JSON.toJSONString(value)), "标识数组" , isNull, 1000, 2, 1, 0, 50);
                        
                                                                            }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String reportDeleteAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
    
                private static String reportListACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("type" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String reportListACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String serveEditACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("port" );
                            list.add("ip" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String serveEditACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String serveUploadACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("ids" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String serveUploadACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String reportUploadACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("fileUrl" );
                            list.add("type" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String reportUploadACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String reportDownloadACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("ids" );
                            list.add("type" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String reportDownloadACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String reportDeleteACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("ids" );
                            list.add("type" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String reportDeleteACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
    
    /**
     * 获取集合中的不同值
     *
     * @param list
     * @param set
     * @return
     */
    private static String getCollectionDifferentValue(List<String> list, Set<String> set, String flag){
        String result = "";
        for (String str : list){
            if (!set.contains(str)){
                result += str + ",";
            }
        }
        if (StringUtils.isNotEmpty(result)){
            result = StringUtils.strip(result, ",");
            result = "---" + flag + ":" + result + "为必填参数";
        }
        return result;
    }
}
