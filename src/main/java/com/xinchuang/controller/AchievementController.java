package com.xinchuang.controller;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.xinchuang.utils.CheckUtil;
import com.xinchuang.comment.R;
import com.xinchuang.comment.ResultCode;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;

import com.xinchuang.utils.DateUtils;
import org.springframework.web.multipart.MultipartFile;
import org.apache.commons.lang3.StringUtils;
import com.xinchuang.service.AchievementService;

/**
 * AchievementController
 *
 * @date 2023-08-25
 */
@RestController
@RequestMapping("/achievement" )
@Api(value = "成果管理", tags = "成果管理")
public class AchievementController {
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss" );

    @Resource
    private AchievementService achievementService;

        /**
     * 成果文档导入接口  函数名称addResultWord  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/addResultWord" )
            @ApiOperation("成果文档导入")
                                    public R addResultWord(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + addResultWordACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = achievementService.addResultWord(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + addResultWordACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * MB属性查询接口  函数名称tarAttribute  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/tarAttribute" )
            @ApiOperation("MB属性查询")
                                    public R tarAttribute(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + tarAttributeACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = achievementService.tarAttribute(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + tarAttributeACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * MB属性编辑接口  函数名称tarAttributeEdit  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/tarAttributeEdit" )
            @ApiOperation("MB属性编辑")
                                    public R tarAttributeEdit(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + tarAttributeEditACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = achievementService.tarAttributeEdit(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + tarAttributeEditACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 子MB属性查询接口  函数名称subAttribute  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/subAttribute" )
            @ApiOperation("子MB属性查询")
                                    public R subAttribute(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + subAttributeACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = achievementService.subAttribute(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + subAttributeACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 子MB属性编辑接口  函数名称subAttributeEdit  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/subAttributeEdit" )
            @ApiOperation("子MB属性编辑")
                                    public R subAttributeEdit(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + subAttributeEditACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = achievementService.subAttributeEdit(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + subAttributeEditACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 要素查询接口  函数名称element  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/element" )
            @ApiOperation("要素查询")
                                    public R element(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + elementACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = achievementService.element(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + elementACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 要素编辑接口  函数名称elementEdit  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/elementEdit" )
            @ApiOperation("要素编辑")
                                    public R elementEdit(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + elementEditACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = achievementService.elementEdit(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + elementEditACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 导出成果报告接口  函数名称exportResult  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/exportResult" )
            @ApiOperation("导出成果报告")
                                    public R exportResult(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + exportResultACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = achievementService.exportResult(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + exportResultACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 类别列表接口  函数名称typeList  功能描述:
                     * @return 返回参数{}
     */
            @PostMapping("/typeList" )
            @ApiOperation("类别列表")
                                    public R typeList() {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    String result = "";
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = achievementService.typeList();
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + typeListACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * MB列表接口  函数名称mbList  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/mbList" )
            @ApiOperation("MB列表")
                                    public R mbList(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + mbListACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = achievementService.mbList(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + mbListACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 编辑类别接口  函数名称typeEdit  功能描述:添加时默认添加到下级
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/typeEdit" )
            @ApiOperation("编辑类别")
                                    public R typeEdit(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + typeEditACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = achievementService.typeEdit(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + typeEditACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 删除类别接口  函数名称typeDelete  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/typeDelete" )
            @ApiOperation("删除类别")
                                    public R typeDelete(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + typeDeleteACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = achievementService.typeDelete(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + typeDeleteACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 单个子MB信息上图查询接口  函数名称subMap  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/subMap" )
            @ApiOperation("单个子MB信息上图查询")
                                    public R subMap(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + subMapACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = achievementService.subMap(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + subMapACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 删除MB子MB接口  函数名称tarDelete  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/tarDelete" )
            @ApiOperation("删除MB子MB")
                                    public R tarDelete(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + tarDeleteACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = achievementService.tarDelete(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + tarDeleteACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 单个MB信息上图查询接口  函数名称tarMap  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/tarMap" )
            @ApiOperation("单个MB信息上图查询")
                                    public R tarMap(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + tarMapACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = achievementService.tarMap(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + tarMapACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 编辑MB子MB接口  函数名称tarEdit  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/tarEdit" )
            @ApiOperation("编辑MB子MB")
                                    public R tarEdit(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + tarEditACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = achievementService.tarEdit(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + tarEditACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 要素类别接口  函数名称eleList  功能描述:
                     * @return 返回参数{}
     */
            @PostMapping("/eleList" )
            @ApiOperation("要素类别")
                                    public R eleList() {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    String result = "";
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = achievementService.eleList();
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + eleListACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 单个MB信息上图编辑接口  函数名称tarMapEdit  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/tarMapEdit" )
            @ApiOperation("单个MB信息上图编辑")
                                    public R tarMapEdit(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + tarMapEditACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = achievementService.tarMapEdit(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + tarMapEditACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 上移下移接口  函数名称upNode  功能描述:type为：1,上移同级顶部；2，上移；3，下移；4，下移同级底部
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/upNode" )
            @ApiOperation("上移下移")
                                    public R upNode(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + upNodeACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = achievementService.upNode(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + upNodeACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

    
    public static String jsonLoop(Object object, String result, String methodName, Set<String> set) {
        Object v = null;
        if (object instanceof JSONArray || object instanceof ArrayList) {
            JSONArray jsonArray = new JSONArray();
            if (object instanceof ArrayList){
                jsonArray = JSONArray.parseArray(JSON.toJSONString(object));
            }else {
                jsonArray = (JSONArray) object;
            }
            for (int i = 0; i < jsonArray.size(); i++) {
                result = jsonLoop(jsonArray.get(i), result, methodName, set);
                if (StringUtils.isNotEmpty(result)) {
                    return result;
                }
            }
        }
        if (object instanceof JSONObject) {
            JSONObject jsonObject = (JSONObject) object;
            for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
                Object o = entry.getValue();
                if (o instanceof JSONArray || o instanceof ArrayList) {
                    result += selectCheckMathod(entry.getKey(), entry.getValue(), methodName, set);
                    result = jsonLoop(o, result, methodName, set);
                    if (StringUtils.isNotEmpty(result)) {
                        return result;
                    }
                } else if (o instanceof JSONObject) {
                    result = jsonLoop(o, result, methodName, set);
                    if (StringUtils.isNotEmpty(result)) {
                        return result;
                    }
                } else {
//                   FIXME: 未知类型
                }
                v = entry.getValue();
                result += selectCheckMathod(entry.getKey(), v, methodName, set);
            }
        }
        return result;
    }

    private static String selectCheckMathod(String key, Object value, String methodName, Set<String> set) {
        String result = "";
                            if ("addResultWordSC".equals(methodName)) {
                result = addResultWordSC(key, value, set);
            }
                    if ("addResultWordAC".equals(methodName)) {
                result = addResultWordAC(key, value, set);
            }
                            if ("tarAttributeSC".equals(methodName)) {
                result = tarAttributeSC(key, value, set);
            }
                    if ("tarAttributeAC".equals(methodName)) {
                result = tarAttributeAC(key, value, set);
            }
                            if ("tarAttributeEditSC".equals(methodName)) {
                result = tarAttributeEditSC(key, value, set);
            }
                    if ("tarAttributeEditAC".equals(methodName)) {
                result = tarAttributeEditAC(key, value, set);
            }
                            if ("subAttributeSC".equals(methodName)) {
                result = subAttributeSC(key, value, set);
            }
                    if ("subAttributeAC".equals(methodName)) {
                result = subAttributeAC(key, value, set);
            }
                            if ("subAttributeEditSC".equals(methodName)) {
                result = subAttributeEditSC(key, value, set);
            }
                    if ("subAttributeEditAC".equals(methodName)) {
                result = subAttributeEditAC(key, value, set);
            }
                            if ("elementSC".equals(methodName)) {
                result = elementSC(key, value, set);
            }
                    if ("elementAC".equals(methodName)) {
                result = elementAC(key, value, set);
            }
                            if ("elementEditSC".equals(methodName)) {
                result = elementEditSC(key, value, set);
            }
                    if ("elementEditAC".equals(methodName)) {
                result = elementEditAC(key, value, set);
            }
                            if ("exportResultSC".equals(methodName)) {
                result = exportResultSC(key, value, set);
            }
                    if ("exportResultAC".equals(methodName)) {
                result = exportResultAC(key, value, set);
            }
                            if ("typeListSC".equals(methodName)) {
                result = typeListSC(key, value, set);
            }
                    if ("typeListAC".equals(methodName)) {
                result = typeListAC(key, value, set);
            }
                            if ("mbListSC".equals(methodName)) {
                result = mbListSC(key, value, set);
            }
                    if ("mbListAC".equals(methodName)) {
                result = mbListAC(key, value, set);
            }
                            if ("typeEditSC".equals(methodName)) {
                result = typeEditSC(key, value, set);
            }
                    if ("typeEditAC".equals(methodName)) {
                result = typeEditAC(key, value, set);
            }
                            if ("typeDeleteSC".equals(methodName)) {
                result = typeDeleteSC(key, value, set);
            }
                    if ("typeDeleteAC".equals(methodName)) {
                result = typeDeleteAC(key, value, set);
            }
                            if ("subMapSC".equals(methodName)) {
                result = subMapSC(key, value, set);
            }
                    if ("subMapAC".equals(methodName)) {
                result = subMapAC(key, value, set);
            }
                            if ("tarDeleteSC".equals(methodName)) {
                result = tarDeleteSC(key, value, set);
            }
                    if ("tarDeleteAC".equals(methodName)) {
                result = tarDeleteAC(key, value, set);
            }
                            if ("tarMapSC".equals(methodName)) {
                result = tarMapSC(key, value, set);
            }
                    if ("tarMapAC".equals(methodName)) {
                result = tarMapAC(key, value, set);
            }
                            if ("tarEditSC".equals(methodName)) {
                result = tarEditSC(key, value, set);
            }
                    if ("tarEditAC".equals(methodName)) {
                result = tarEditAC(key, value, set);
            }
                            if ("eleListSC".equals(methodName)) {
                result = eleListSC(key, value, set);
            }
                    if ("eleListAC".equals(methodName)) {
                result = eleListAC(key, value, set);
            }
                            if ("tarMapEditSC".equals(methodName)) {
                result = tarMapEditSC(key, value, set);
            }
                    if ("tarMapEditAC".equals(methodName)) {
                result = tarMapEditAC(key, value, set);
            }
                            if ("upNodeSC".equals(methodName)) {
                result = upNodeSC(key, value, set);
            }
                    if ("upNodeAC".equals(methodName)) {
                result = upNodeAC(key, value, set);
            }
                return result;
    }


                private static String addResultWordSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("level".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "层级" , isNull, "0", "50");
                                                                                                                                                                                                }
                                        if ("typeId".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("fileUrl".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "文件地址" , isNull, 0, 999);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String addResultWordAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String tarAttributeSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("tarId".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String tarAttributeAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("tarSign".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "MB标识", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("tarCode".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "MB识别码", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("country".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "国家地区", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("tarRegion".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "MB区", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("tarName".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "MB名称", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("tarSerial".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "MB编号", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("tarType".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "类别", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("tarGradient".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "MB等级", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                                        if ("tarReliability".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "成果可信度", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("coordinateSystem".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "坐标系", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("subPosition".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "定位点子MB", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("position".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "定位点", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("lon".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "经度（度分秒）", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("lat".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "纬度（度分秒）", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("geoHeight".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "大地高度（米）", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("altitude".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "海拔高度（米）", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("area".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "面积（平方米）", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("remark".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "备注", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("pointError".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "点位误差（米）", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("lonError".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "经度误差（秒）", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("latError".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "纬度误差（秒）", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("geoHeightError".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "大地高误差（米）", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("altitudeError".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "海拔高误差（米）", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("masterMapError".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "原图误差（米）", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("tarMapError".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "图制作误差（米）", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("plotError".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "地物加绘误差（米）", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("resultClassification".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "成果密级", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("reorganizeUnit".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "整编单位", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("reorganizeDate".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "整编日期", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("publicationNumber".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "出版序号", isNull, 0, 50);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String tarAttributeEditSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("tarId".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("tarSign".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "MB标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("tarCode".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "MB识别码" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("country".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "国家地区" , isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("tarRegion".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "MB区" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("tarName".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "MB名称" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("tarSerial".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "MB编号" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("tarType".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "类别" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("tarGradient".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "MB等级" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("tarReliability".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "成果可信度" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("coordinateSystem".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "坐标系" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("subPosition".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "定位点子MB" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("position".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "定位点" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("lon".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "经度（度分秒）" , isNull, "0", "99999");
                                                                                                                                                }
                                        if ("lat".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "纬度（度分秒）" , isNull, "0", "99999");
                                                                                                                                                }
                                        if ("geoHeight".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "大地高度（米）" , isNull, "0", "99999");
                                                                                                                                                }
                                        if ("altitude".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "海拔高度（米）" , isNull, "0", "99999");
                                                                                                                                                }
                                        if ("area".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "面积（平方米）" , isNull, "0", "99999");
                                                                                                                                                }
                                        if ("remark".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "备注" , isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("pointError".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "点位误差（米）" , isNull, "0", "99999");
                                                                                                                                                }
                                        if ("lonError".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "经度误差（秒）" , isNull, "0", "99999");
                                                                                                                                                }
                                        if ("latError".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "纬度误差（秒）" , isNull, "0", "99999");
                                                                                                                                                }
                                        if ("geoHeightError".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "大地高误差（米）" , isNull, "0", "99999");
                                                                                                                                                }
                                        if ("altitudeError".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "海拔高误差（米）" , isNull, "0", "99999");
                                                                                                                                                }
                                        if ("masterMapError".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "原图误差（米）" , isNull, "0", "99999");
                                                                                                                                                }
                                        if ("tarMapError".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "图制作误差（米）" , isNull, "0", "99999");
                                                                                                                                                }
                                        if ("plotError".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "地物加绘误差（米）" , isNull, "0", "99999");
                                                                                                                                                }
                                        if ("resultClassification".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "成果密级" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("reorganizeUnit".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "整编单位" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("reorganizeDate".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "整编日期" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("publicationNumber".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "出版序号" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String tarAttributeEditAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String subAttributeSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("subId".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String subAttributeAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                                        if ("subSign".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "子MB标识", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("subName".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "名称", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("subCode".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识码", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("subType".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "类型", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("materialType".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "材质类型", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("structureType".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "结构类型", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("length".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "长度（米）", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("wide".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "宽度（米）", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("height".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "高度（米）", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("azimuthAngle".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "方位角（度）", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("inRadius".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "圆内半径（米）", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("outRadius".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "圆外半径（米）", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("coordinateSystem".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "坐标系", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("position".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "定位点", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("lon".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "经度（度分秒）", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("lat".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "纬度（度分秒）", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("geoHeight".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "大地高度（米）", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("altitude".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "海拔高度（米）", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("area".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "面积（平方米）", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("remark".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "备注", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("pointError".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "点位误差（米）", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("lonError".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "经度误差（秒）", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("latError".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "纬度误差（秒）", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("geoHeightError".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "大地高误差（米）", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("altitudeError".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "海拔高误差（米）", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("masterMapError".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "原图误差（米）", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("tarMapError".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "图制作误差（米）", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("plotError".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "地物加绘误差（米）", isNull, 0, 50);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String subAttributeEditSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("subId".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("subSign".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "子MB标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("subName".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "名称" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("subCode".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识码" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("subType".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "类型" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("materialType".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "材质类型" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("structureType".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "结构类型" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("length".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "长度（米）" , isNull, "0", "99999");
                                                                                                                                                }
                                        if ("wide".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "宽度（米）" , isNull, "0", "99999");
                                                                                                                                                }
                                        if ("height".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "高度（米）" , isNull, "0", "99999");
                                                                                                                                                }
                                        if ("azimuthAngle".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "方位角（度）" , isNull, "0", "99999");
                                                                                                                                                }
                                        if ("inRadius".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "圆内半径（米）" , isNull, "0", "99999");
                                                                                                                                                }
                                        if ("outRadius".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "圆外半径（米）" , isNull, "0", "99999");
                                                                                                                                                }
                                        if ("coordinateSystem".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "坐标系" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("position".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "定位点" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("lon".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "经度（度分秒）" , isNull, "0", "99999");
                                                                                                                                                }
                                        if ("lat".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "纬度（度分秒）" , isNull, "0", "99999");
                                                                                                                                                }
                                        if ("geoHeight".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "大地高度（米）" , isNull, "0", "99999");
                                                                                                                                                }
                                        if ("altitude".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "海拔高度（米）" , isNull, "0", "99999");
                                                                                                                                                }
                                        if ("area".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "面积（平方米）" , isNull, "0", "99999");
                                                                                                                                                }
                                        if ("remark".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "备注" , isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("pointError".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "点位误差（米）" , isNull, "0", "99999");
                                                                                                                                                }
                                        if ("lonError".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "经度误差（秒）" , isNull, "0", "99999");
                                                                                                                                                }
                                        if ("latError".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "纬度误差（秒）" , isNull, "0", "99999");
                                                                                                                                                }
                                        if ("geoHeightError".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "大地高误差（米）" , isNull, "0", "99999");
                                                                                                                                                }
                                        if ("altitudeError".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "海拔高误差（米）" , isNull, "0", "99999");
                                                                                                                                                }
                                        if ("masterMapError".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "原图误差（米）" , isNull, "0", "99999");
                                                                                                                                                }
                                        if ("tarMapError".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "图制作误差（米）" , isNull, "0", "99999");
                                                                                                                                                }
                                        if ("plotError".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "地物加绘误差（米）" , isNull, "0", "99999");
                                                                                                                                                }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String subAttributeEditAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String elementSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("tarId".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("eleId".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String elementAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                                        if ("eleContent".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "要素内容", isNull, 0, 99999);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String elementEditSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("tarId".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("eleId".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("eleContent".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "要素内容" , isNull, 0, 99999);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String elementEditAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String exportResultSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("tarId".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String exportResultAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                                        if ("fileUrl".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "文件", isNull, 0, 255);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String typeListSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String typeListAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                                        if ("data".equals(key)) {
                                                                                                                        result += CheckUtil.checkJSONArray(JSONArray.parseArray(JSON.toJSONString(value)), "承载数据", isNull);
                                    }
                                        if ("level".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "层级", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("typeId".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("typeName".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "名称", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("children".equals(key)) {
                                                                                                                        result += CheckUtil.checkJSONArray(JSONArray.parseArray(JSON.toJSONString(value)), "下级列表", isNull);
                                    }
                                        if ("level".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "层级", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("typeId".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("typeName".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "名称", isNull, 0, 50);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String mbListSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("typeId".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "类别" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("tarName".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "名称" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String mbListAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                                        if ("data".equals(key)) {
                                                                                                                        result += CheckUtil.checkJSONArray(JSONArray.parseArray(JSON.toJSONString(value)), "承载数据", isNull);
                                    }
                                        if ("tarId".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("tarName".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "MB名称", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("children".equals(key)) {
                                                                                                                        result += CheckUtil.checkJSONArray(JSONArray.parseArray(JSON.toJSONString(value)), "子MB列表", isNull);
                                    }
                                        if ("subId".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("subName".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "子MB名称", isNull, 0, 50);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String typeEditSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("level".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "层级" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("typeId".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("typeName".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "名称" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String typeEditAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String typeDeleteSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("level".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "层级" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("typeId".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String typeDeleteAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String subMapSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("tarId".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "MB标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("subId".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "子MB标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String subMapAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                                        if ("subId".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "子MB标识", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("subName".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "名称", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("position".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "位置", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("plotList".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标绘信息", isNull, 0, 99999);
                                                                                                                                                                                                                        }
                                        if ("outPlot".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标绘列表", isNull, 0, 99999);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String tarDeleteSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("id".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String tarDeleteAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String tarMapSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("tarId".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "MB标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String tarMapAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                                        if ("tarId".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "MB标识", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("tarName".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "名称", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("position".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "位置", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("plotList".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标绘信息", isNull, 0, 99999);
                                                                                                                                                                                                                        }
                                        if ("outPlot".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标绘列表", isNull, 0, 99999);
                                                                                                                                                                                                                        }
                                        if ("tarType".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "类型", isNull, 0, 50);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String tarEditSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("id".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("name".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "名称" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String tarEditAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String eleListSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String eleListAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                                        if ("data".equals(key)) {
                                                                                                                        result += CheckUtil.checkJSONArray(JSONArray.parseArray(JSON.toJSONString(value)), "承载数据", isNull);
                                    }
                                        if ("eleName".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "名称", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("eleId".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识", isNull, 0, 50);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String tarMapEditSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("tarId".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("position".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "位置" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("plotList".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标绘信息" , isNull, 0, 99999);
                                                                                                                                                                                                                        }
                                        if ("outPlot".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标绘列表" , isNull, 0, 99999);
                                                                                                                                                                                                                        }
                                        if ("tarName".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "名称" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("tarType".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "类型" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String tarMapEditAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String upNodeSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("type".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "类型" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String upNodeAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
    
                private static String addResultWordACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("typeId" );
                            list.add("fileUrl" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String addResultWordACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String tarAttributeACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("tarId" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String tarAttributeACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String tarAttributeEditACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("tarId" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String tarAttributeEditACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String subAttributeACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("subId" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String subAttributeACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String subAttributeEditACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("subId" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String subAttributeEditACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String elementACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("tarId" );
                            list.add("eleId" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String elementACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String elementEditACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("tarId" );
                            list.add("eleId" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String elementEditACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String exportResultACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("tarId" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String exportResultACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String typeListACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String typeListACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String mbListACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("typeId" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String mbListACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String typeEditACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("typeId" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String typeEditACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String typeDeleteACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("typeId" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String typeDeleteACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String subMapACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("tarId" );
                            list.add("subId" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String subMapACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String tarDeleteACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("id" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String tarDeleteACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String tarMapACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("tarId" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String tarMapACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String tarEditACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("id" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String tarEditACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String eleListACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String eleListACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String tarMapEditACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String tarMapEditACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String upNodeACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String upNodeACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
    
    /**
     * 获取集合中的不同值
     *
     * @param list
     * @param set
     * @return
     */
    private static String getCollectionDifferentValue(List<String> list, Set<String> set, String flag){
        String result = "";
        for (String str : list){
            if (!set.contains(str)){
                result += str + ",";
            }
        }
        if (StringUtils.isNotEmpty(result)){
            result = StringUtils.strip(result, ",");
            result = "---" + flag + ":" + result + "为必填参数";
        }
        return result;
    }
}
