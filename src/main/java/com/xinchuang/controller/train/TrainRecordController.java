package com.xinchuang.controller.train;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@RestController
@RequestMapping("/train-record")
public class TrainRecordController {

}
