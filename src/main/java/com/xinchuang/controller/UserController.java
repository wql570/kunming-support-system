package com.xinchuang.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xinchuang.comment.R;
import com.xinchuang.service.UserService;
import com.xinchuang.utils.CheckUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * UserController
 *
 * @date 2023-08-25
 */
@RestController
@RequestMapping("/user")
@Api(value = "用户管理", tags = "用户管理")
public class UserController {
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Resource
    private UserService userService;

    /**
     * 单个用户接口  函数名称user  功能描述:
     *
     * @param jsonObject 中文名:通用实体类
     * @return 返回参数{}
     */
    @PostMapping("/user")
    @ApiOperation("单个用户")
    public R user(@RequestBody JSONObject jsonObject) {
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        Set<String> sSet = new HashSet<>();
        String result = jsonLoop(jsonObject, "", methodName + "SC", sSet);
        result = result + userACS(sSet);
        if (result.length() > 0) {
            return R.fail("入参：" + result);
        } else {
            // 调用业务层代码
            String r = userService.user(jsonObject);
            Set<String> aSet = new HashSet<>();
            JSONObject o = JSON.parseObject(r);
            if (o == null) {
                o = new JSONObject();
            }
            result = jsonLoop(o.get("data"), "", methodName + "AC", aSet);
            result = result + userACB(aSet);
            if (result.length() > 0) {
                return R.fail("出参：" + result);
            }
            return JSON.parseObject(r, R.class);
        }
    }

    /**
     * 用户列表接口  函数名称userList  功能描述:
     *
     * @param jsonObject 中文名:通用实体类
     * @return 返回参数{}
     */
    @PostMapping("/userList")
    @ApiOperation("用户列表")
    public R userList(@RequestBody JSONObject jsonObject) {
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        Set<String> sSet = new HashSet<>();
        String result = jsonLoop(jsonObject, "", methodName + "SC", sSet);
        result = result + userListACS(sSet);
        if (result.length() > 0) {
            return R.fail("入参：" + result);
        } else {
            // 调用业务层代码
            String r = userService.userList(jsonObject);
            Set<String> aSet = new HashSet<>();
            JSONObject o = JSON.parseObject(r);
            if (o == null) {
                o = new JSONObject();
            }
            result = jsonLoop(o.get("data"), "", methodName + "AC", aSet);
            result = result + userListACB(aSet);
            if (result.length() > 0) {
                return R.fail("出参：" + result);
            }
            return JSON.parseObject(r, R.class);
        }
    }

    /**
     * 编辑用户接口  函数名称updateUser  功能描述:
     *
     * @param jsonObject 中文名:通用实体类
     * @return 返回参数{}
     */
    @PostMapping("/updateUser")
    @ApiOperation("编辑用户")
    public R updateUser(@RequestBody JSONObject jsonObject) {
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        Set<String> sSet = new HashSet<>();
        String result = jsonLoop(jsonObject, "", methodName + "SC", sSet);
        result = result + updateUserACS(sSet);
        if (result.length() > 0) {
            return R.fail("入参：" + result);
        } else {
            // 调用业务层代码
            String r = userService.updateUser(jsonObject);
            Set<String> aSet = new HashSet<>();
            JSONObject o = JSON.parseObject(r);
            if (o == null) {
                o = new JSONObject();
            }
            result = jsonLoop(o.get("data"), "", methodName + "AC", aSet);
            result = result + updateUserACB(aSet);
            if (result.length() > 0) {
                return R.fail("出参：" + result);
            }
            return JSON.parseObject(r, R.class);
        }
    }

    /**
     * 删除用户接口  函数名称deleteUser  功能描述:
     *
     * @param jsonObject 中文名:通用实体类
     * @return 返回参数{}
     */
    @PostMapping("/deleteUser")
    @ApiOperation("删除用户")
    public R deleteUser(@RequestBody JSONObject jsonObject) {
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        Set<String> sSet = new HashSet<>();
        String result = jsonLoop(jsonObject, "", methodName + "SC", sSet);
        result = result + deleteUserACS(sSet);
        if (result.length() > 0) {
            return R.fail("入参：" + result);
        } else {
            // 调用业务层代码
            String r = userService.deleteUser(jsonObject);
            Set<String> aSet = new HashSet<>();
            JSONObject o = JSON.parseObject(r);
            if (o == null) {
                o = new JSONObject();
            }
            result = jsonLoop(o.get("data"), "", methodName + "AC", aSet);
            result = result + deleteUserACB(aSet);
            if (result.length() > 0) {
                return R.fail("出参：" + result);
            }
            return JSON.parseObject(r, R.class);
        }
    }


    public static String jsonLoop(Object object, String result, String methodName, Set<String> set) {
        Object v = null;
        if (object instanceof JSONArray || object instanceof ArrayList) {
            JSONArray jsonArray = new JSONArray();
            if (object instanceof ArrayList) {
                jsonArray = JSONArray.parseArray(JSON.toJSONString(object));
            } else {
                jsonArray = (JSONArray) object;
            }
            for (int i = 0; i < jsonArray.size(); i++) {
                result = jsonLoop(jsonArray.get(i), result, methodName, set);
                if (StringUtils.isNotEmpty(result)) {
                    return result;
                }
            }
        }
        if (object instanceof JSONObject) {
            JSONObject jsonObject = (JSONObject) object;
            for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
                Object o = entry.getValue();
                if (o instanceof JSONArray || o instanceof ArrayList) {
                    result += selectCheckMathod(entry.getKey(), entry.getValue(), methodName, set);
                    result = jsonLoop(o, result, methodName, set);
                    if (StringUtils.isNotEmpty(result)) {
                        return result;
                    }
                } else if (o instanceof JSONObject) {
                    result = jsonLoop(o, result, methodName, set);
                    if (StringUtils.isNotEmpty(result)) {
                        return result;
                    }
                } else {
//                   FIXME: 未知类型
                }
                v = entry.getValue();
                result += selectCheckMathod(entry.getKey(), v, methodName, set);
            }
        }
        return result;
    }

    private static String selectCheckMathod(String key, Object value, String methodName, Set<String> set) {
        String result = "";
        if ("userSC".equals(methodName)) {
            result = userSC(key, value, set);
        }
        if ("userAC".equals(methodName)) {
            result = userAC(key, value, set);
        }
        if ("userListSC".equals(methodName)) {
            result = userListSC(key, value, set);
        }
        if ("userListAC".equals(methodName)) {
            result = userListAC(key, value, set);
        }
        if ("updateUserSC".equals(methodName)) {
            result = updateUserSC(key, value, set);
        }
        if ("updateUserAC".equals(methodName)) {
            result = updateUserAC(key, value, set);
        }
        if ("deleteUserSC".equals(methodName)) {
            result = deleteUserSC(key, value, set);
        }
        if ("deleteUserAC".equals(methodName)) {
            result = deleteUserAC(key, value, set);
        }
        return result;
    }


    private static String userSC(String key, Object value, Set<String> set) {
        String result = "";
        Boolean isNull = false;
        if ("id".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "标识", isNull, 0, 50);
        }
        if (isNull) {
            set.add(key);
        }
        return result;
    }

    private static String userAC(String key, Object value, Set<String> set) {
        String result = "";
        Boolean isNull = false;
        if ("code".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkBigDecimal(value == null ? "" : value.toString(), "状态码", isNull, "0", "100000");
        }
        if ("msg".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "返回消息", isNull, 0, 255);
        }
        if ("success".equals(key)) {
            // 判断参数类型: String  num boolean datetime
        }
        if ("id".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "标识", isNull, 0, 50);
        }
        if ("userName".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "用户名", isNull, 0, 50);
        }
        if ("xName".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "姓名", isNull, 0, 50);
        }
        if ("role".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "类型", isNull, 0, 50);
        }
        if ("unit".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "单位", isNull, 0, 50);
        }
        if ("phone".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "联系方式", isNull, 0, 50);
        }
        if ("account".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "登录账号", isNull, 0, 50);
        }
        if ("password".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "登录密码", isNull, 0, 50);
        }
        if ("passwordAgin".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "确认密码", isNull, 0, 50);
        }
        if ("remark".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "备注", isNull, 0, 255);
        }
        if (isNull) {
            set.add(key);
        }
        return result;
    }

    private static String userListSC(String key, Object value, Set<String> set) {
        String result = "";
        Boolean isNull = false;
        if ("pageNum".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkBigDecimal(value == null ? "" : value.toString(), "页码", isNull, "0", "99999");
        }
        if ("pageSize".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkBigDecimal(value == null ? "" : value.toString(), "页容", isNull, "0", "99999");
        }
        if ("keyword".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "关键字", isNull, 0, 50);
        }
        if (isNull) {
            set.add(key);
        }
        return result;
    }

    private static String userListAC(String key, Object value, Set<String> set) {
        String result = "";
        Boolean isNull = false;
        if ("code".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkBigDecimal(value == null ? "" : value.toString(), "状态码", isNull, "0", "100000");
        }
        if ("msg".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "返回消息", isNull, 0, 255);
        }
        if ("success".equals(key)) {
            // 判断参数类型: String  num boolean datetime
        }
        if ("id".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "标识", isNull, 0, 50);
        }
        if ("userName".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "用户名", isNull, 0, 50);
        }
        if ("xName".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "姓名", isNull, 0, 50);
        }
        if ("role".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "类型", isNull, 0, 50);
        }
        if ("unit".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "单位", isNull, 0, 50);
        }
        if ("phone".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "联系方式", isNull, 0, 50);
        }
        if ("remark".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "备注", isNull, 0, 50);
        }
        if ("createTime".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "创建时间", isNull, 0, 50);
        }
        if ("createName".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "创建人", isNull, 0, 50);
        }
        if ("userList".equals(key)) {
            result += CheckUtil.checkJSONArray(JSONArray.parseArray(JSON.toJSONString(value)), "用户列表", isNull);
        }
        if ("pageNum".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkBigDecimal(value == null ? "" : value.toString(), "页码", isNull, "0", "99999");
        }
        if ("pageSize".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkBigDecimal(value == null ? "" : value.toString(), "页容", isNull, "0", "99999");
        }
        if ("pageAll".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkBigDecimal(value == null ? "" : value.toString(), "页总", isNull, "0", "99999");
        }
        if ("total".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkBigDecimal(value == null ? "" : value.toString(), "总条数", isNull, "0", "99999");
        }
        if (isNull) {
            set.add(key);
        }
        return result;
    }

    private static String updateUserSC(String key, Object value, Set<String> set) {
        String result = "";
        Boolean isNull = false;
        if ("id".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "标识", isNull, 0, 50);
        }
        if ("userName".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "用户名", isNull, 0, 50);
        }
        if ("xName".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "姓名", isNull, 0, 50);
        }
        if ("role".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "类型", isNull, 0, 50);
        }
        if ("unit".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "单位", isNull, 0, 50);
        }
        if ("phone".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "联系方式", isNull, 0, 50);
        }
        if ("account".equals(key)) {
            isNull = true;
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "登录账号", isNull, 0, 50);
        }
        if ("password".equals(key)) {
            isNull = true;
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "登录密码", isNull, 0, 50);
        }
        if ("passwordAgin".equals(key)) {
            isNull = true;
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "确认密码", isNull, 0, 50);
        }
        if (isNull) {
            set.add(key);
        }
        return result;
    }

    private static String updateUserAC(String key, Object value, Set<String> set) {
        String result = "";
        Boolean isNull = false;
        if ("code".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkBigDecimal(value == null ? "" : value.toString(), "状态码", isNull, "0", "100000");
        }
        if ("msg".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "返回消息", isNull, 0, 255);
        }
        if ("success".equals(key)) {
            // 判断参数类型: String  num boolean datetime
        }
        if (isNull) {
            set.add(key);
        }
        return result;
    }

    private static String deleteUserSC(String key, Object value, Set<String> set) {
        String result = "";
        Boolean isNull = false;
        if ("ids".equals(key)) {
            isNull = true;
            // 判断参数类型: String  num
            result += CheckUtil.checkJSONArray(JSONArray.parseArray(JSON.toJSONString(value)), "标识列表", isNull, 1000, 2, 1, 0, 50);

        }
        if (isNull) {
            set.add(key);
        }
        return result;
    }

    private static String deleteUserAC(String key, Object value, Set<String> set) {
        String result = "";
        Boolean isNull = false;
        if ("code".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkBigDecimal(value == null ? "" : value.toString(), "状态码", isNull, "0", "100000");
        }
        if ("msg".equals(key)) {
            // 判断参数类型: String  num boolean datetime
            result += CheckUtil.checkString(value == null ? "" : value.toString(), "返回消息", isNull, 0, 255);
        }
        if ("success".equals(key)) {
            // 判断参数类型: String  num boolean datetime
        }
        if (isNull) {
            set.add(key);
        }
        return result;
    }

    private static String userACS(Set<String> set) {
        List<String> list = new ArrayList<>();
        if (set.size() == list.size()) {
            return "";
        }
        return getCollectionDifferentValue(list, set, "入参");
    }

    private static String userACB(Set<String> set) {
        List<String> list = new ArrayList<>();
        if (set.size() == list.size()) {
            return "";
        }
        return getCollectionDifferentValue(list, set, "出参");
    }

    private static String userListACS(Set<String> set) {
        List<String> list = new ArrayList<>();
        if (set.size() == list.size()) {
            return "";
        }
        return getCollectionDifferentValue(list, set, "入参");
    }

    private static String userListACB(Set<String> set) {
        List<String> list = new ArrayList<>();
        if (set.size() == list.size()) {
            return "";
        }
        return getCollectionDifferentValue(list, set, "出参");
    }

    private static String updateUserACS(Set<String> set) {
        List<String> list = new ArrayList<>();
        list.add("password");
        list.add("passwordAgin");
        list.add("account");
        if (set.size() == list.size()) {
            return "";
        }
        return getCollectionDifferentValue(list, set, "入参");
    }

    private static String updateUserACB(Set<String> set) {
        List<String> list = new ArrayList<>();
        if (set.size() == list.size()) {
            return "";
        }
        return getCollectionDifferentValue(list, set, "出参");
    }

    private static String deleteUserACS(Set<String> set) {
        List<String> list = new ArrayList<>();
        list.add("ids");
        if (set.size() == list.size()) {
            return "";
        }
        return getCollectionDifferentValue(list, set, "入参");
    }

    private static String deleteUserACB(Set<String> set) {
        List<String> list = new ArrayList<>();
        if (set.size() == list.size()) {
            return "";
        }
        return getCollectionDifferentValue(list, set, "出参");
    }

    /**
     * 获取集合中的不同值
     *
     * @param list
     * @param set
     * @return
     */
    private static String getCollectionDifferentValue(List<String> list, Set<String> set, String flag) {
        String result = "";
        for (String str : list) {
            if (!set.contains(str)) {
                result += str + ",";
            }
        }
        if (StringUtils.isNotEmpty(result)) {
            result = StringUtils.strip(result, ",");
            result = "---" + flag + ":" + result + "为必填参数";
        }
        return result;
    }
}
