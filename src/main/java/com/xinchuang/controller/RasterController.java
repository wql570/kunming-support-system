package com.xinchuang.controller;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.xinchuang.utils.CheckUtil;
import com.xinchuang.comment.R;
import com.xinchuang.comment.ResultCode;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;

import com.xinchuang.utils.DateUtils;
import org.springframework.web.multipart.MultipartFile;
import org.apache.commons.lang3.StringUtils;
import com.xinchuang.service.RasterService;

/**
 * RasterController
 *
 * @date 2023-08-25
 */
@RestController
@RequestMapping("/raster" )
@Api(value = "MB栅格", tags = "MB栅格")
public class RasterController {
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss" );

    @Resource
    private RasterService rasterService;

        /**
     * 影像列表接口  函数名称imgList  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/imgList" )
            @ApiOperation("影像列表")
                                    public R imgList(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + imgListACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = rasterService.imgList(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + imgListACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 单个影像查询接口  函数名称img  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/img" )
            @ApiOperation("单个影像查询")
                                    public R img(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + imgACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = rasterService.img(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + imgACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 自动栅格编辑接口  函数名称autoRaster  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/autoRaster" )
            @ApiOperation("自动栅格编辑")
                                    public R autoRaster(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + autoRasterACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = rasterService.autoRaster(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + autoRasterACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 编辑标绘接口  函数名称editPlotList  功能描述:获取回来之后，对编辑之后的信息进行保存
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/editPlotList" )
            @ApiOperation("编辑标绘")
                                    public R editPlotList(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + editPlotListACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = rasterService.editPlotList(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + editPlotListACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

    
    public static String jsonLoop(Object object, String result, String methodName, Set<String> set) {
        Object v = null;
        if (object instanceof JSONArray || object instanceof ArrayList) {
            JSONArray jsonArray = new JSONArray();
            if (object instanceof ArrayList){
                jsonArray = JSONArray.parseArray(JSON.toJSONString(object));
            }else {
                jsonArray = (JSONArray) object;
            }
            for (int i = 0; i < jsonArray.size(); i++) {
                result = jsonLoop(jsonArray.get(i), result, methodName, set);
                if (StringUtils.isNotEmpty(result)) {
                    return result;
                }
            }
        }
        if (object instanceof JSONObject) {
            JSONObject jsonObject = (JSONObject) object;
            for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
                Object o = entry.getValue();
                if (o instanceof JSONArray || o instanceof ArrayList) {
                    result += selectCheckMathod(entry.getKey(), entry.getValue(), methodName, set);
                    result = jsonLoop(o, result, methodName, set);
                    if (StringUtils.isNotEmpty(result)) {
                        return result;
                    }
                } else if (o instanceof JSONObject) {
                    result = jsonLoop(o, result, methodName, set);
                    if (StringUtils.isNotEmpty(result)) {
                        return result;
                    }
                } else {
//                   FIXME: 未知类型
                }
                v = entry.getValue();
                result += selectCheckMathod(entry.getKey(), v, methodName, set);
            }
        }
        return result;
    }

    private static String selectCheckMathod(String key, Object value, String methodName, Set<String> set) {
        String result = "";
                            if ("imgListSC".equals(methodName)) {
                result = imgListSC(key, value, set);
            }
                    if ("imgListAC".equals(methodName)) {
                result = imgListAC(key, value, set);
            }
                            if ("imgSC".equals(methodName)) {
                result = imgSC(key, value, set);
            }
                    if ("imgAC".equals(methodName)) {
                result = imgAC(key, value, set);
            }
                            if ("autoRasterSC".equals(methodName)) {
                result = autoRasterSC(key, value, set);
            }
                    if ("autoRasterAC".equals(methodName)) {
                result = autoRasterAC(key, value, set);
            }
                            if ("editPlotListSC".equals(methodName)) {
                result = editPlotListSC(key, value, set);
            }
                    if ("editPlotListAC".equals(methodName)) {
                result = editPlotListAC(key, value, set);
            }
                return result;
    }


                private static String imgListSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("tarId".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("pageNum".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "页码" , isNull, "0", "99999");
                                                                                                                                                                                                }
                                        if ("pageSize".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "页容" , isNull, "0", "99999");
                                                                                                                                                                                                }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String imgListAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                                        if ("imgName".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "影像名称", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("tarId".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("updateTime".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "时间", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("userName".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "用户", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("pageNum".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "页码", isNull, "0", "99999");
                                                                                                                                                                                                }
                                        if ("pageSize".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "页容", isNull, "0", "99999");
                                                                                                                                                                                                }
                                        if ("pageAll".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "页总", isNull, "0", "99999");
                                                                                                                                                                                                }
                                        if ("total".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "总条数", isNull, "0", "99999");
                                                                                                                                                                                                }
                                        if ("imgList".equals(key)) {
                                                                                                                        result += CheckUtil.checkJSONArray(JSONArray.parseArray(JSON.toJSONString(value)), "影像列表", isNull);
                                    }
                                        if ("thuUrl".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "缩略图", isNull, 0, 999);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String imgSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("tarId".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String imgAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                                        if ("imgUrl".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "影像", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("plotlist".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标绘", isNull, 0, 255);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String autoRasterSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("tarId".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("plotlist".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标绘" , isNull, 0, 99999);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String autoRasterAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String editPlotListSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("tarId".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("plotStr".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标绘信息" , isNull, 0, 99999);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String editPlotListAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
    
                private static String imgListACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("tarId" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String imgListACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String imgACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("tarId" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String imgACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String autoRasterACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("tarId" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String autoRasterACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String editPlotListACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String editPlotListACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
    
    /**
     * 获取集合中的不同值
     *
     * @param list
     * @param set
     * @return
     */
    private static String getCollectionDifferentValue(List<String> list, Set<String> set, String flag){
        String result = "";
        for (String str : list){
            if (!set.contains(str)){
                result += str + ",";
            }
        }
        if (StringUtils.isNotEmpty(result)){
            result = StringUtils.strip(result, ",");
            result = "---" + flag + ":" + result + "为必填参数";
        }
        return result;
    }
}
