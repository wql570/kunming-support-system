package com.xinchuang.controller;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.xinchuang.utils.CheckUtil;
import com.xinchuang.comment.R;
import com.xinchuang.comment.ResultCode;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;

import com.xinchuang.utils.DateUtils;
import org.springframework.web.multipart.MultipartFile;
import org.apache.commons.lang3.StringUtils;
import com.xinchuang.service.RoleService;

/**
 * RoleController
 *
 * @date 2023-08-25
 */
@RestController
@RequestMapping("/role" )
@Api(value = "角色管理", tags = "角色管理")
public class RoleController {
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss" );

    @Resource
    private RoleService roleService;

        /**
     * 角色列表接口  函数名称roleList  功能描述:
                     * @return 返回参数{}
     */
            @PostMapping("/roleList" )
            @ApiOperation("角色列表")
                                    public R roleList() {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    String result = "";
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = roleService.roleList();
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + roleListACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 菜单列表接口  函数名称navList  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/navList" )
            @ApiOperation("菜单列表")
                                    public R navList(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + navListACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = roleService.navList(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + navListACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 角色授权接口  函数名称roleAccredit  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/roleAccredit" )
            @ApiOperation("角色授权")
                                    public R roleAccredit(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + roleAccreditACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = roleService.roleAccredit(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + roleAccreditACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

        /**
     * 全部授权接口  函数名称accreditAll  功能描述:
         * @param jsonObject 中文名:通用实体类
                 * @return 返回参数{}
     */
            @PostMapping("/accreditAll" )
            @ApiOperation("全部授权")
                                    public R accreditAll(@RequestBody JSONObject jsonObject) {
                                    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
                                    Set<String> sSet = new HashSet<>();
                                            String result = jsonLoop(jsonObject, "" , methodName + "SC" , sSet);
                                        result = result + accreditAllACS(sSet);
                                if (result.length() > 0) {
                    return R.fail("入参：" + result);
                } else {
                    // 调用业务层代码
                                                                        String r = roleService.accreditAll(jsonObject);
                                                                Set<String> aSet = new HashSet<>();
                    JSONObject o = JSON.parseObject(r);
                    if (o == null){
                        o = new JSONObject();
                    }
                    result = jsonLoop(o.get("data"), "" , methodName + "AC" , aSet);
                    result = result + accreditAllACB(aSet);
                    if (result.length() > 0) {
                        return R.fail("出参：" + result);
                    }
                    return JSON.parseObject(r, R.class);
                }
            }

    
    public static String jsonLoop(Object object, String result, String methodName, Set<String> set) {
        Object v = null;
        if (object instanceof JSONArray || object instanceof ArrayList) {
            JSONArray jsonArray = new JSONArray();
            if (object instanceof ArrayList){
                jsonArray = JSONArray.parseArray(JSON.toJSONString(object));
            }else {
                jsonArray = (JSONArray) object;
            }
            for (int i = 0; i < jsonArray.size(); i++) {
                result = jsonLoop(jsonArray.get(i), result, methodName, set);
                if (StringUtils.isNotEmpty(result)) {
                    return result;
                }
            }
        }
        if (object instanceof JSONObject) {
            JSONObject jsonObject = (JSONObject) object;
            for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
                Object o = entry.getValue();
                if (o instanceof JSONArray || o instanceof ArrayList) {
                    result += selectCheckMathod(entry.getKey(), entry.getValue(), methodName, set);
                    result = jsonLoop(o, result, methodName, set);
                    if (StringUtils.isNotEmpty(result)) {
                        return result;
                    }
                } else if (o instanceof JSONObject) {
                    result = jsonLoop(o, result, methodName, set);
                    if (StringUtils.isNotEmpty(result)) {
                        return result;
                    }
                } else {
//                   FIXME: 未知类型
                }
                v = entry.getValue();
                result += selectCheckMathod(entry.getKey(), v, methodName, set);
            }
        }
        return result;
    }

    private static String selectCheckMathod(String key, Object value, String methodName, Set<String> set) {
        String result = "";
                            if ("roleListSC".equals(methodName)) {
                result = roleListSC(key, value, set);
            }
                    if ("roleListAC".equals(methodName)) {
                result = roleListAC(key, value, set);
            }
                            if ("navListSC".equals(methodName)) {
                result = navListSC(key, value, set);
            }
                    if ("navListAC".equals(methodName)) {
                result = navListAC(key, value, set);
            }
                            if ("roleAccreditSC".equals(methodName)) {
                result = roleAccreditSC(key, value, set);
            }
                    if ("roleAccreditAC".equals(methodName)) {
                result = roleAccreditAC(key, value, set);
            }
                            if ("accreditAllSC".equals(methodName)) {
                result = accreditAllSC(key, value, set);
            }
                    if ("accreditAllAC".equals(methodName)) {
                result = accreditAllAC(key, value, set);
            }
                return result;
    }


                private static String roleListSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String roleListAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                                        if ("data".equals(key)) {
                                                                                                                        result += CheckUtil.checkJSONArray(JSONArray.parseArray(JSON.toJSONString(value)), "承载数据", isNull);
                                    }
                                        if ("id".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "标识", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("role".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "类型", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("remark".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "备注", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("url".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "图", isNull, 0, 999);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String navListSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("role".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "类型" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String navListAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("data".equals(key)) {
                                                                                                                        result += CheckUtil.checkJSONArray(JSONArray.parseArray(JSON.toJSONString(value)), "承载数据", isNull);
                                    }
                                        if ("subSysName".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "子系统名称", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("accredit".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "授权", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("id".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "子系统标识", isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("subSysEName".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "英文名称", isNull, 0, 50);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String roleAccreditSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("role".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "角色" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("subSysName".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "子系统标识" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                                        if ("accredit".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String roleAccreditAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
                private static String accreditAllSC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("accreditAll".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                                        if ("role".equals(key)) {
                                                                                        isNull = true;
                                                // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "类型" , isNull, 0, 50);
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
            private static String accreditAllAC(String key, Object value, Set<String> set) {
            String result = "";
            Boolean isNull = false;
                                                if ("code".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                            result += CheckUtil.checkBigDecimal(value == null?"":value.toString(), "状态码", isNull, "0", "100000");
                                                                                                                                                                                                }
                                        if ("msg".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                    result += CheckUtil.checkString(value == null?"":value.toString(), "返回消息", isNull, 0, 255);
                                                                                                                                                                                                                        }
                                        if ("success".equals(key)) {
                                                                                    // 判断参数类型: String  num boolean datetime
                                                                                                                                                                                                                        }
                        if (isNull) {
                set.add(key);
            }
            return result;
        }
    
                private static String roleListACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String roleListACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String navListACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("role" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String navListACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String roleAccreditACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("role" );
                            list.add("subSysName" );
                            list.add("accredit" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String roleAccreditACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
                private static String accreditAllACS(Set<String> set) {
            List<String> list = new ArrayList<>();
                            list.add("role" );
                            list.add("accreditAll" );
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "入参");
        }
            private static String accreditAllACB(Set<String> set) {
            List<String> list = new ArrayList<>();
                        if (set.size() == list.size()) {
                return "";
            }
            return getCollectionDifferentValue(list, set, "出参");
        }
    
    /**
     * 获取集合中的不同值
     *
     * @param list
     * @param set
     * @return
     */
    private static String getCollectionDifferentValue(List<String> list, Set<String> set, String flag){
        String result = "";
        for (String str : list){
            if (!set.contains(str)){
                result += str + ",";
            }
        }
        if (StringUtils.isNotEmpty(result)){
            result = StringUtils.strip(result, ",");
            result = "---" + flag + ":" + result + "为必填参数";
        }
        return result;
    }
}
