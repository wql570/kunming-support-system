package com.xinchuang.service;

import com.alibaba.fastjson.JSONObject;
import com.xinchuang.comment.R;
import org.springframework.web.multipart.MultipartFile;

public interface ValueAnalyseService {

                                        String mbValueList(JSONObject jsonObject);
                    
                                        String subValueList(JSONObject jsonObject);
                    
                                        String mbValueEdit(JSONObject jsonObject);
                    
                                        String subValueEdit(JSONObject jsonObject);
                    
                                        String knoValueEdit(JSONObject jsonObject);
                    
    
}
