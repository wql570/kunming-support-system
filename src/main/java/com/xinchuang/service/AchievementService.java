package com.xinchuang.service;

import com.alibaba.fastjson.JSONObject;
import com.xinchuang.comment.R;
import org.springframework.web.multipart.MultipartFile;

public interface AchievementService {

                                        String addResultWord(JSONObject jsonObject);
                    
                                        String tarAttribute(JSONObject jsonObject);
                    
                                        String tarAttributeEdit(JSONObject jsonObject);
                    
                                        String subAttribute(JSONObject jsonObject);
                    
                                        String subAttributeEdit(JSONObject jsonObject);
                    
                                        String element(JSONObject jsonObject);
                    
                                        String elementEdit(JSONObject jsonObject);
                    
                                        String exportResult(JSONObject jsonObject);
                    
                                        String typeList();
                    
                                        String mbList(JSONObject jsonObject);
                    
                                        String typeEdit(JSONObject jsonObject);
                    
                                        String typeDelete(JSONObject jsonObject);
                    
                                        String subMap(JSONObject jsonObject);
                    
                                        String tarDelete(JSONObject jsonObject);
                    
                                        String tarMap(JSONObject jsonObject);
                    
                                        String tarEdit(JSONObject jsonObject);
                    
                                        String eleList();
                    
                                        String tarMapEdit(JSONObject jsonObject);
                    
                                        String upNode(JSONObject jsonObject);
                    
    
}
