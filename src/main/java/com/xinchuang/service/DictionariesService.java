package com.xinchuang.service;

import com.alibaba.fastjson.JSONObject;
import com.xinchuang.comment.R;
import org.springframework.web.multipart.MultipartFile;

public interface DictionariesService {

                                        String attributeList(JSONObject jsonObject);
                    
                                        String attributeEdit(JSONObject jsonObject);
                    
                                        String attributeDelete(JSONObject jsonObject);
                    
                                        String typeList();
                    
                                        String typeEdit();
                    
                                        String typeDelete();
                    
                                        String typeAttributeList(JSONObject jsonObject);
                    
                                        String typeAttributeEdit(JSONObject jsonObject);
                    
                                        String typeAttributeDelete(JSONObject jsonObject);
                    
                                        String knowList(JSONObject jsonObject);
                    
                                        String knowEdit(JSONObject jsonObject);
                    
                                        String knowDelete(JSONObject jsonObject);
                    
                                        String upDownSpec(JSONObject jsonObject);
                    
                                        String upDown(JSONObject jsonObject);
                    
    
}
