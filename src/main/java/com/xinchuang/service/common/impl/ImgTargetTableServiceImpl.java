package com.xinchuang.service.common.impl;

import com.xinchuang.entity.common.ImgTargetTable;
import com.xinchuang.mapper.common.ImgTargetTableMapper;
import com.xinchuang.service.common.ImgTargetTableService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@Service
public class ImgTargetTableServiceImpl extends ServiceImpl<ImgTargetTableMapper, ImgTargetTable> implements ImgTargetTableService {

}
