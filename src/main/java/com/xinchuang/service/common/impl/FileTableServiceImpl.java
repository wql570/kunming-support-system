package com.xinchuang.service.common.impl;

import com.xinchuang.entity.common.FileTable;
import com.xinchuang.mapper.common.FileTableMapper;
import com.xinchuang.service.common.FileTableService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@Service
public class FileTableServiceImpl extends ServiceImpl<FileTableMapper, FileTable> implements FileTableService {

}
