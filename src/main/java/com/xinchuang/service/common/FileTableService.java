package com.xinchuang.service.common;

import com.xinchuang.entity.common.FileTable;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
public interface FileTableService extends IService<FileTable> {

}
