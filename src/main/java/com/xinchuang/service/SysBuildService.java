package com.xinchuang.service;

import com.alibaba.fastjson.JSONObject;
import com.xinchuang.comment.R;
import org.springframework.web.multipart.MultipartFile;

public interface SysBuildService {

                                        String sysList(JSONObject jsonObject);
                    
                                        String sysInfo(JSONObject jsonObject);
                    
                                        String sysDelete(JSONObject jsonObject);
                    
                                        String sysInfoEdit();
                    
                                        String relationEdit(JSONObject jsonObject);
                    
                                        String mbWaitList();
                    
    
}
