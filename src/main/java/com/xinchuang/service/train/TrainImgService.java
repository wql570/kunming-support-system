package com.xinchuang.service.train;

import com.xinchuang.entity.train.TrainImg;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
public interface TrainImgService extends IService<TrainImg> {

}
