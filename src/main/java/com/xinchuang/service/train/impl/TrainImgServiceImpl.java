package com.xinchuang.service.train.impl;

import com.xinchuang.entity.train.TrainImg;
import com.xinchuang.mapper.train.TrainImgMapper;
import com.xinchuang.service.train.TrainImgService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@Service
public class TrainImgServiceImpl extends ServiceImpl<TrainImgMapper, TrainImg> implements TrainImgService {

}
