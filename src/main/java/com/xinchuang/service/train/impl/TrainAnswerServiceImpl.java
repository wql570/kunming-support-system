package com.xinchuang.service.train.impl;

import com.xinchuang.entity.train.TrainAnswer;
import com.xinchuang.mapper.train.TrainAnswerMapper;
import com.xinchuang.service.train.TrainAnswerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@Service
public class TrainAnswerServiceImpl extends ServiceImpl<TrainAnswerMapper, TrainAnswer> implements TrainAnswerService {

}
