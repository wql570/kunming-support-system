package com.xinchuang.service.train.impl;

import com.xinchuang.entity.train.TrainRecord;
import com.xinchuang.mapper.train.TrainRecordMapper;
import com.xinchuang.service.train.TrainRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@Service
public class TrainRecordServiceImpl extends ServiceImpl<TrainRecordMapper, TrainRecord> implements TrainRecordService {

}
