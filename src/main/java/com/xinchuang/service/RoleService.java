package com.xinchuang.service;

import com.alibaba.fastjson.JSONObject;
import com.xinchuang.comment.R;
import org.springframework.web.multipart.MultipartFile;

public interface RoleService {

                                        String roleList();
                    
                                        String navList(JSONObject jsonObject);
                    
                                        String roleAccredit(JSONObject jsonObject);
                    
                                        String accreditAll(JSONObject jsonObject);
                    
    
}
