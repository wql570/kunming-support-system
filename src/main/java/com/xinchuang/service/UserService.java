package com.xinchuang.service;

import com.alibaba.fastjson.JSONObject;

public interface UserService {

    String user(JSONObject jsonObject);

    String userList(JSONObject jsonObject);

    String updateUser(JSONObject jsonObject);

    String deleteUser(JSONObject jsonObject);


}
