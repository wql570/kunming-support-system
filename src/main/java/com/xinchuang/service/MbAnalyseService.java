package com.xinchuang.service;

import com.alibaba.fastjson.JSONObject;
import com.xinchuang.comment.R;
import org.springframework.web.multipart.MultipartFile;

public interface MbAnalyseService {

                                        String mbInfo(JSONObject jsonObject);
                    
                                        String analyseImportant(JSONObject jsonObject);
                    
                                        String analyseCalc(JSONObject jsonObject);
                    
                                        String analyseReportExport(JSONObject jsonObject);
                    
                                        String printscreen(JSONObject jsonObject);
                    
                                        String plotSave(JSONObject jsonObject);
                    
    
}
