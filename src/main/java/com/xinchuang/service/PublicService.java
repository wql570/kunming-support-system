package com.xinchuang.service;

import com.alibaba.fastjson.JSONObject;
import com.xinchuang.comment.R;
import org.springframework.web.multipart.MultipartFile;

public interface PublicService {

                                        String uploadFile(
                                                                     MultipartFile fileData
                                    );
                    
                                        String uploadImg(
                                                                     MultipartFile imgData
                                    );
                    
                                        String countryList(JSONObject jsonObject);
                    
                                        String login(JSONObject jsonObject);
                    
    
}
