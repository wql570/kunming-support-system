package com.xinchuang.service;

import com.alibaba.fastjson.JSONObject;
import com.xinchuang.comment.R;
import org.springframework.web.multipart.MultipartFile;

public interface RasterService {

                                        String imgList(JSONObject jsonObject);
                    
                                        String img(JSONObject jsonObject);
                    
                                        String autoRaster(JSONObject jsonObject);
                    
                                        String editPlotList(JSONObject jsonObject);
                    
    
}
