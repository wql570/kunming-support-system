package com.xinchuang.service;

import com.alibaba.fastjson.JSONObject;
import com.xinchuang.comment.R;
import org.springframework.web.multipart.MultipartFile;

public interface PhotoService {

                                        String imgList(JSONObject jsonObject);
                    
                                        String imgUpload(JSONObject jsonObject);
                    
                                        String plotEdit(JSONObject jsonObject);
                    
                                        String imgDelete(JSONObject jsonObject);
                    
    
}
