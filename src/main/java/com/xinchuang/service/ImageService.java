package com.xinchuang.service;

import com.alibaba.fastjson.JSONObject;
import com.xinchuang.comment.R;
import org.springframework.web.multipart.MultipartFile;

public interface ImageService {

                                        String reslutList(JSONObject jsonObject);
                    
                                        String correctResult(JSONObject jsonObject);
                    
                                        String recognitionResult(JSONObject jsonObject);
                    
                                        String rasterResult(JSONObject jsonObject);
                    
                                        String resultDelete(JSONObject jsonObject);
                    
                                        String resultExport(JSONObject jsonObject);
                    
    
}
