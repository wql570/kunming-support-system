package com.xinchuang.service;

import com.alibaba.fastjson.JSONObject;
import com.xinchuang.comment.R;
import org.springframework.web.multipart.MultipartFile;

public interface PushService {

                                        String reportList(JSONObject jsonObject);
                    
                                        String serveEdit(JSONObject jsonObject);
                    
                                        String serveUpload(JSONObject jsonObject);
                    
                                        String reportUpload(JSONObject jsonObject);
                    
                                        String reportDownload(JSONObject jsonObject);
                    
                                        String reportDelete(JSONObject jsonObject);
                    
    
}
