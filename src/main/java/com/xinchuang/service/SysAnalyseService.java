package com.xinchuang.service;

import com.alibaba.fastjson.JSONObject;
import com.xinchuang.comment.R;
import org.springframework.web.multipart.MultipartFile;

public interface SysAnalyseService {

                                        String sysList(JSONObject jsonObject);
                    
                                        String mbPlotInfo(JSONObject jsonObject);
                    
                                        String subList(JSONObject jsonObject);
                    
                                        String mbList(JSONObject jsonObject);
                    
                                        String sysInfo(JSONObject jsonObject);
                    
                                        String analyseImportant();
                    
                                        String planeReach(JSONObject jsonObject);
                    
                                        String lineAnalyse(JSONObject jsonObject);
                    
                                        String planeUsable(JSONObject jsonObject);
                    
                                        String analyseReportExport(JSONObject jsonObject);
                    
    
}
