package com.xinchuang.service;

import com.alibaba.fastjson.JSONObject;
import com.xinchuang.comment.R;
import org.springframework.web.multipart.MultipartFile;

public interface ReorganizeService {

                                        String autoCorrect(JSONObject jsonObject);
                    
                                        String imgUpload(JSONObject jsonObject);
                    
                                        String serImgList(JSONObject jsonObject);
                    
                                        String othImgList();
                    
                                        String pointCorrect(JSONObject jsonObject);
                    
                                        String test(JSONObject jsonObject);
                    
                                        String exportPrecision(JSONObject jsonObject);
                    
                                        String precisionSave(JSONObject jsonObject);
                    
                                        String correctSave(JSONObject jsonObject);
                    
                                        String correctList(JSONObject jsonObject);
                    
                                        String autoRecognition(JSONObject jsonObject);
                    
                                        String exportMb(JSONObject jsonObject);
                    
                                        String mbSave(JSONObject jsonObject);
                    
                                        String recognitionSave(JSONObject jsonObject);
                    
                                        String subject(JSONObject jsonObject);
                    
                                        String subjectEdit(JSONObject jsonObject);
                    
                                        String griddingEdit(JSONObject jsonObject);
                    
                                        String typeList();
                    
                                        String typeEdit(JSONObject jsonObject);
                    
                                        String subList(JSONObject jsonObject);
                    
                                        String subEdit(JSONObject jsonObject);
                    
                                        String typeDelete(JSONObject jsonObject);
                    
                                        String subDelete(JSONObject jsonObject);
                    
                                        String subPlot(JSONObject jsonObject);
                    
                                        String editMbFast(JSONObject jsonObject);
                    
                                        String tarAttributeEditFast(JSONObject jsonObject);
                    
                                        String upNodeFast(JSONObject jsonObject);
                    
                                        String subAttributeEditFast(JSONObject jsonObject);
                    
                                        String tarAttributeFast(JSONObject jsonObject);
                    
                                        String subAttributeFast(JSONObject jsonObject);
                    
    
}
