package com.xinchuang.service.repacigi;

import com.xinchuang.entity.repacigi.RepacigiMainTable;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
public interface RepacigiMainTableService extends IService<RepacigiMainTable> {

}
