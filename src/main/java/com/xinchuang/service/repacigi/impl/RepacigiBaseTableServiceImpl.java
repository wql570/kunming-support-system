package com.xinchuang.service.repacigi.impl;

import com.xinchuang.entity.repacigi.RepacigiBaseTable;
import com.xinchuang.mapper.repacigi.RepacigiBaseTableMapper;
import com.xinchuang.service.repacigi.RepacigiBaseTableService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@Service
public class RepacigiBaseTableServiceImpl extends ServiceImpl<RepacigiBaseTableMapper, RepacigiBaseTable> implements RepacigiBaseTableService {

}
