package com.xinchuang.service.repacigi.impl;

import com.xinchuang.entity.repacigi.RepacigiSubTable;
import com.xinchuang.mapper.repacigi.RepacigiSubTableMapper;
import com.xinchuang.service.repacigi.RepacigiSubTableService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@Service
public class RepacigiSubTableServiceImpl extends ServiceImpl<RepacigiSubTableMapper, RepacigiSubTable> implements RepacigiSubTableService {

}
