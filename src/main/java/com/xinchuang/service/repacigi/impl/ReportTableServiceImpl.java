package com.xinchuang.service.repacigi.impl;

import com.xinchuang.entity.repacigi.ReportTable;
import com.xinchuang.mapper.repacigi.ReportTableMapper;
import com.xinchuang.service.repacigi.ReportTableService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@Service
public class ReportTableServiceImpl extends ServiceImpl<ReportTableMapper, ReportTable> implements ReportTableService {

}
