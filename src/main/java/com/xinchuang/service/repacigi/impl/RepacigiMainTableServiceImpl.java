package com.xinchuang.service.repacigi.impl;

import com.xinchuang.entity.repacigi.RepacigiMainTable;
import com.xinchuang.mapper.repacigi.RepacigiMainTableMapper;
import com.xinchuang.service.repacigi.RepacigiMainTableService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@Service
public class RepacigiMainTableServiceImpl extends ServiceImpl<RepacigiMainTableMapper, RepacigiMainTable> implements RepacigiMainTableService {

}
