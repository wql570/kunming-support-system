package com.xinchuang.service.target;

import com.xinchuang.entity.target.TargetSubTable;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
public interface TargetSubTableService extends IService<TargetSubTable> {

}
