package com.xinchuang.service.target.impl;

import com.xinchuang.entity.target.TargetCommonAttribute;
import com.xinchuang.mapper.target.TargetCommonAttributeMapper;
import com.xinchuang.service.target.TargetCommonAttributeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@Service
public class TargetCommonAttributeServiceImpl extends ServiceImpl<TargetCommonAttributeMapper, TargetCommonAttribute> implements TargetCommonAttributeService {

}
