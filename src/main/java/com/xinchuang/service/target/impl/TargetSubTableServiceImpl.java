package com.xinchuang.service.target.impl;

import com.xinchuang.entity.target.TargetSubTable;
import com.xinchuang.mapper.target.TargetSubTableMapper;
import com.xinchuang.service.target.TargetSubTableService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@Service
public class TargetSubTableServiceImpl extends ServiceImpl<TargetSubTableMapper, TargetSubTable> implements TargetSubTableService {

}
