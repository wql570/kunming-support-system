package com.xinchuang.service.target.impl;

import com.xinchuang.entity.target.TargetType;
import com.xinchuang.mapper.target.TargetTypeMapper;
import com.xinchuang.service.target.TargetTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@Service
public class TargetTypeServiceImpl extends ServiceImpl<TargetTypeMapper, TargetType> implements TargetTypeService {

}
