package com.xinchuang.service.target.impl;

import com.xinchuang.entity.target.TargetSpecialAttribute;
import com.xinchuang.mapper.target.TargetSpecialAttributeMapper;
import com.xinchuang.service.target.TargetSpecialAttributeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@Service
public class TargetSpecialAttributeServiceImpl extends ServiceImpl<TargetSpecialAttributeMapper, TargetSpecialAttribute> implements TargetSpecialAttributeService {

}
