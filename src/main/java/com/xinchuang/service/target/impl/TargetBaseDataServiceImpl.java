package com.xinchuang.service.target.impl;

import com.xinchuang.entity.target.TargetBaseData;
import com.xinchuang.mapper.target.TargetBaseDataMapper;
import com.xinchuang.service.target.TargetBaseDataService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@Service
public class TargetBaseDataServiceImpl extends ServiceImpl<TargetBaseDataMapper, TargetBaseData> implements TargetBaseDataService {

}
