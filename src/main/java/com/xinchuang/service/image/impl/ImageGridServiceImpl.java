package com.xinchuang.service.image.impl;

import com.xinchuang.entity.image.ImageGrid;
import com.xinchuang.mapper.image.ImageGridMapper;
import com.xinchuang.service.image.ImageGridService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@Service
public class ImageGridServiceImpl extends ServiceImpl<ImageGridMapper, ImageGrid> implements ImageGridService {

}
