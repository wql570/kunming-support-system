package com.xinchuang.service.image.impl;

import com.xinchuang.entity.image.ImageSpecialSubject;
import com.xinchuang.mapper.image.ImageSpecialSubjectMapper;
import com.xinchuang.service.image.ImageSpecialSubjectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@Service
public class ImageSpecialSubjectServiceImpl extends ServiceImpl<ImageSpecialSubjectMapper, ImageSpecialSubject> implements ImageSpecialSubjectService {

}
