package com.xinchuang.service.image.impl;

import com.xinchuang.entity.image.ImageResult;
import com.xinchuang.mapper.image.ImageResultMapper;
import com.xinchuang.service.image.ImageResultService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@Service
public class ImageResultServiceImpl extends ServiceImpl<ImageResultMapper, ImageResult> implements ImageResultService {

}
