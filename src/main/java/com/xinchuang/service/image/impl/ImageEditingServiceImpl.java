package com.xinchuang.service.image.impl;

import com.xinchuang.entity.image.ImageEditing;
import com.xinchuang.mapper.image.ImageEditingMapper;
import com.xinchuang.service.image.ImageEditingService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@Service
public class ImageEditingServiceImpl extends ServiceImpl<ImageEditingMapper, ImageEditing> implements ImageEditingService {

}
