package com.xinchuang.service.image;

import com.xinchuang.entity.image.ImageResult;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
public interface ImageResultService extends IService<ImageResult> {

}
