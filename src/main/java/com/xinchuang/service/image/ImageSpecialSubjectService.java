package com.xinchuang.service.image;

import com.xinchuang.entity.image.ImageSpecialSubject;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
public interface ImageSpecialSubjectService extends IService<ImageSpecialSubject> {

}
