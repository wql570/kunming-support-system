package com.xinchuang.service;

import com.alibaba.fastjson.JSONObject;
import com.xinchuang.comment.R;
import org.springframework.web.multipart.MultipartFile;

public interface MbManageService {

                                        String mbList(JSONObject jsonObject);
                    
                                        String subList(JSONObject jsonObject);
                    
                                        String mbInfo(JSONObject jsonObject);
                    
                                        String subInfo(JSONObject jsonObject);
                    
                                        String mbDetail(JSONObject jsonObject);
                    
                                        String subDetail(JSONObject jsonObject);
                    
                                        String mbInfoEdit(JSONObject jsonObject);
                    
                                        String subInfoEdit(JSONObject jsonObject);
                    
                                        String mbDetailEdit(JSONObject jsonObject);
                    
                                        String subDetailEdit(JSONObject jsonObject);
                    
                                        String mbDelete(JSONObject jsonObject);
                    
                                        String subDelete(JSONObject jsonObject);
                    
    
}
