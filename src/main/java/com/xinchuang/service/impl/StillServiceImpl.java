package com.xinchuang.service.impl;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xinchuang.comment.R;
import com.xinchuang.service.StillService;
import com.xinchuang.utils.CheckUtil;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.text.SimpleDateFormat;
import com.xinchuang.utils.DateUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSON;

@Service
public class StillServiceImpl implements StillService {

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                                        @Override
                public String imgList(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标绘信息
                    String region = jsonObject.getString("region");
                                                                                                                    if (
                                                                                                                                                                        "标绘的一个区域的边界的字符串".equals(region)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":[{\"imgName\":\"影像名称\",\"imgId\":\"xxxx\",\"thuUrl\":\"缩略图地址\",\"type\":\"影像类型\"}],\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String img(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取影像标识
                    String imgId = jsonObject.getString("imgId");
                                                                                                                    if (
                                                                                                                                                                        "xxxxxxxx".equals(imgId)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{\"imgUrl\":\"影像地址\",\"centerPosition\":[\"1\",\"1\",\"0\"]},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String tarPlotList() {
                                                                                            if (1 ==1
                                            ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":[{\"plotList\":\"标绘字符串\"}],\"success\":true}";
                                                    }
                                                            if (1 ==1
                                            ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
    
}
