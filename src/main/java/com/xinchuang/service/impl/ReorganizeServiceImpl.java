package com.xinchuang.service.impl;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xinchuang.comment.R;
import com.xinchuang.service.ReorganizeService;
import com.xinchuang.utils.CheckUtil;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.text.SimpleDateFormat;
import com.xinchuang.utils.DateUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSON;

@Service
public class ReorganizeServiceImpl implements ReorganizeService {

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                                        @Override
                public String autoCorrect(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String tarId = jsonObject.getString("tarId");
                                                                                                                    if (
                                                                                                                                                                        "xxx".equals(tarId)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{\"newUrl\":\"xxxx\",\"plotList\":\"aaaaaa\",\"url\":\"xxx\"},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String imgUpload(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String tarId = jsonObject.getString("tarId");
                                                                                //jsonobject解析获取名称
                    String imgName = jsonObject.getString("imgName");
                                                                                //jsonobject解析获取文件地址
                    String imgUrl = jsonObject.getString("imgUrl");
                                                                                //jsonobject解析获取类型
                    String type = jsonObject.getString("type");
                                                                                                                    if (
                                                                                                                                                                        "xxx".equals(tarId)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "影像名称".equals(imgName)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "www.baidu.com".equals(imgUrl)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "0".equals(type)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String serImgList(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取页码
                    String pageNum = jsonObject.getString("pageNum");
                                                                                //jsonobject解析获取页容
                    String pageSize = jsonObject.getString("pageSize");
                                                                                //jsonobject解析获取名称
                    String fileName = jsonObject.getString("fileName");
                                                                                                                    if (
                                                                                                                                                                        "1".equals(pageNum)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "10".equals(pageSize)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "影像名字".equals(fileName)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{\"total\":50,\"pageAll\":5,\"pageSize\":10,\"pageNum\":1,\"imgList\":[{\"imgName\":\"imgName\",\"imgUrl\":\"www.baidu.com\",\"id\":\"xxx\",\"uploadTime\":\"2012-01-10\"}]},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String othImgList() {
                                                                                            if (1 ==1
                                            ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{},\"success\":true}";
                                                    }
                                                            if (1 ==1
                                            ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String pointCorrect(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String tarId = jsonObject.getString("tarId");
                                                                                //jsonobject解析获取控制点
                    String pointList = jsonObject.getString("pointList");
                                                                                                                    if (
                                                                                                                                                                        "xxx".equals(tarId)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "xxxxxxxx".equals(pointList)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{\"newUrl\":\"aaaaaaaaaaa\",\"plotList\":\"dddddddddd\",\"url\":\"ssssss\"},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String test(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String tarId = jsonObject.getString("tarId");
                                                                                                                    if (
                                                                                                                                                                        "xxx".equals(tarId)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{\"newUrl\":\"new\",\"url\":\"ww\"},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String exportPrecision(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String tarId = jsonObject.getString("tarId");
                                                                                                                    if (
                                                                                                                                                                        "xxx".equals(tarId)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{\"fileUrl\":\"xxxxxxxxx\"},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String precisionSave(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String tarId = jsonObject.getString("tarId");
                                                                                                                    if (
                                                                                                                                                                        "xxx".equals(tarId)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String correctSave(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String tarId = jsonObject.getString("tarId");
                                                                                //jsonobject解析获取标绘信息
                    String plotList = jsonObject.getString("plotList");
                                                                                                                    if (
                                                                                                                                                                        "xxx".equals(tarId)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "xxxxxxxx".equals(plotList)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String correctList(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取页码
                    String pageNum = jsonObject.getString("pageNum");
                                                                                //jsonobject解析获取页容
                    String pageSize = jsonObject.getString("pageSize");
                                                                                //jsonobject解析获取名称
                    String imgName = jsonObject.getString("imgName");
                                                                                //jsonobject解析获取类型
                    String type = jsonObject.getString("type");
                                                                                                                    if (
                                                                                                                                                                        "1".equals(pageNum)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "10".equals(pageSize)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "name".equals(imgName)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "2".equals(type)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{\"imgList\":[{}]},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String autoRecognition(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String tarId = jsonObject.getString("tarId");
                                                                                                                    if (
                                                                                                                                                                        "xxx".equals(tarId)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":[{\"subId\":\"sss\",\"subName\":\"asad\",\"plotList\":\"xxxxxxxxx\"}],\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String exportMb(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String tarId = jsonObject.getString("tarId");
                                                                                                                    if (
                                                                                                                                                                        "xxx".equals(tarId)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{\"fileUrl\":\"xxxxxxxx\"},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String mbSave(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String tarId = jsonObject.getString("tarId");
                                                                                                                    if (
                                                                                                                                                                        "xxx".equals(tarId)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String recognitionSave(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String tarId = jsonObject.getString("tarId");
                                                                                //jsonobject解析获取标绘信息
                    String plotList = jsonObject.getString("plotList");
                                                                                                                    if (
                                                                                                                                                                        "xxx".equals(tarId)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "xxxxxxxxxxxxxxxx".equals(plotList)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String subject(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String tarId = jsonObject.getString("tarId");
                                                                                                                    if (
                                                                                                                                                                        "xxx".equals(tarId)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{\"imgUrl\":\"图片地址\",\"tarId\":\"xxx\",\"scale\":\"比例尺\",\"summarize\":\"概述内容\",\"plotList\":\"标绘信息\",\"interval\":\"间隔\",\"subjectName\":\"专题名称\"},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String subjectEdit(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String tarId = jsonObject.getString("tarId");
                                                                                //jsonobject解析获取概述
                    String summarize = jsonObject.getString("summarize");
                                                                                //jsonobject解析获取名称
                    String subjectName = jsonObject.getString("subjectName");
                                                                                //jsonobject解析获取比例尺
                    String scale = jsonObject.getString("scale");
                                                                                //jsonobject解析获取图地址
                    String imgUrl = jsonObject.getString("imgUrl");
                                                                                //jsonobject解析获取标绘信息
                    String plotList = jsonObject.getString("plotList");
                                                                                //jsonobject解析获取版式
                    String format = jsonObject.getString("format");
                                                                                                                    if (
                                                                                                                                                                        "概述".equals(summarize)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "专题名".equals(subjectName)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "比例尺".equals(scale)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "图片地址".equals(imgUrl)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "标绘字符串信息".equals(plotList)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String griddingEdit(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String tarId = jsonObject.getString("tarId");
                                                                                //jsonobject解析获取间隔
                    String interval = jsonObject.getString("interval");
                                                                                                                    if (
                                                                                                                                                                        "xxx".equals(tarId)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "间隔".equals(interval)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String typeList() {
                                                                                            if (1 ==1
                                            ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":[{\"level\":\"0\",\"children\":[{\"level\":\"1\",\"typeName\":\"二级名称\",\"typeId\":\"xxxx\"}],\"typeName\":\"一级名称\",\"typeId\":\"xxx\"}],\"success\":true}";
                                                    }
                                                            if (1 ==1
                                            ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String typeEdit(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取层级
                    String level = jsonObject.getString("level");
                                                                                //jsonobject解析获取标识
                    String typeId = jsonObject.getString("typeId");
                                                                                //jsonobject解析获取名称
                    String typeName = jsonObject.getString("typeName");
                                                                                                                    if (
                                                                                                                                                                        "1".equals(level)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "xxx".equals(typeId)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "改名字".equals(typeName)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String subList(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取MB标识
                    String tarId = jsonObject.getString("tarId");
                                                                                                                    if (
                                                                                                                                                                        "xxx".equals(tarId)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":[{}],\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String subEdit(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取MB标识
                    String tarId = jsonObject.getString("tarId");
                                                                                //jsonobject解析获取子MB标识
                    String subId = jsonObject.getString("subId");
                                                                                //jsonobject解析获取子MB名称
                    String subName = jsonObject.getString("subName");
                                                                                                                    if (
                                                                                                                                                                        "xxx".equals(tarId)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "xxxx".equals(subId)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "子名称".equals(subName)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String typeDelete(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取层级
                    String level = jsonObject.getString("level");
                                                                                //jsonobject解析获取标识
                    String typeId = jsonObject.getString("typeId");
                                                                                                                    if (
                                                                                                                                                                        "1".equals(level)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "xxxx".equals(typeId)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String subDelete(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取MB标识
                    String tarId = jsonObject.getString("tarId");
                                                                                //jsonobject解析获取子MB标识
                    String subId = jsonObject.getString("subId");
                                                                                                                    if (
                                                                                                                                                                        "xxx".equals(tarId)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "xxxx".equals(subId)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String subPlot(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取MB标识
                    String tarId = jsonObject.getString("tarId");
                                                                                //jsonobject解析获取子MB标识
                    String subId = jsonObject.getString("subId");
                                                                                //jsonobject解析获取标绘信息
                    String plotList = jsonObject.getString("plotList");
                                                                                                                    if (
                                                                                                                                                                        "xxx".equals(tarId)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "xxxx".equals(subId)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "标绘信息".equals(plotList)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String editMbFast(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取名称
                    String typeName = jsonObject.getString("typeName");
                                                                                //jsonobject解析获取标识
                    String typeId = jsonObject.getString("typeId");
                                                                                //jsonobject解析获取层级
                    String level = jsonObject.getString("level");
                                                                                                                    if (
                                                                                                                                                                        "xxx".equals(typeId)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "1".equals(level)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "改名字".equals(typeName)
                                                                                                                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String tarAttributeEditFast(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String tarId = jsonObject.getString("tarId");
                                                                                //jsonobject解析获取MB标识
                    String tarSign = jsonObject.getString("tarSign");
                                                                                //jsonobject解析获取MB识别码
                    String tarCode = jsonObject.getString("tarCode");
                                                                                //jsonobject解析获取国家地区
                    String country = jsonObject.getString("country");
                                                                                //jsonobject解析获取MB区
                    String tarRegion = jsonObject.getString("tarRegion");
                                                                                //jsonobject解析获取MB名称
                    String tarName = jsonObject.getString("tarName");
                                                                                //jsonobject解析获取MB编号
                    String tarSerial = jsonObject.getString("tarSerial");
                                                                                //jsonobject解析获取类别
                    String tarType = jsonObject.getString("tarType");
                                                                                //jsonobject解析获取MB等级
                    String tarGradient = jsonObject.getString("tarGradient");
                                                                                //jsonobject解析获取成果可信度
                    String tarReliability = jsonObject.getString("tarReliability");
                                                                                //jsonobject解析获取坐标系
                    String coordinateSystem = jsonObject.getString("coordinateSystem");
                                                                                //jsonobject解析获取定位点子MB
                    String subPosition = jsonObject.getString("subPosition");
                                                                                //jsonobject解析获取定位点
                    String position = jsonObject.getString("position");
                                                                                //jsonobject解析获取经度（度分秒）
                    String lon = jsonObject.getString("lon");
                                                                                //jsonobject解析获取纬度（度分秒）
                    String lat = jsonObject.getString("lat");
                                                                                //jsonobject解析获取大地高度（米）
                    String geoHeight = jsonObject.getString("geoHeight");
                                                                                //jsonobject解析获取海拔高度（米）
                    String altitude = jsonObject.getString("altitude");
                                                                                //jsonobject解析获取面积（平方米）
                    String area = jsonObject.getString("area");
                                                                                //jsonobject解析获取备注
                    String remark = jsonObject.getString("remark");
                                                                                //jsonobject解析获取点位误差（米）
                    String pointError = jsonObject.getString("pointError");
                                                                                //jsonobject解析获取经度误差（秒）
                    String lonError = jsonObject.getString("lonError");
                                                                                //jsonobject解析获取纬度误差（秒）
                    String latError = jsonObject.getString("latError");
                                                                                //jsonobject解析获取大地高误差（米）
                    String geoHeightError = jsonObject.getString("geoHeightError");
                                                                                //jsonobject解析获取海拔高误差（米）
                    String altitudeError = jsonObject.getString("altitudeError");
                                                                                //jsonobject解析获取原图误差（米）
                    String masterMapError = jsonObject.getString("masterMapError");
                                                                                //jsonobject解析获取图制作误差（米）
                    String tarMapError = jsonObject.getString("tarMapError");
                                                                                //jsonobject解析获取地物加绘误差（米）
                    String plotError = jsonObject.getString("plotError");
                                                                                //jsonobject解析获取成果密级
                    String resultClassification = jsonObject.getString("resultClassification");
                                                                                //jsonobject解析获取整编单位
                    String reorganizeUnit = jsonObject.getString("reorganizeUnit");
                                                                                //jsonobject解析获取整编日期
                    String reorganizeDate = jsonObject.getString("reorganizeDate");
                                                                                //jsonobject解析获取出版序号
                    String publicationNumber = jsonObject.getString("publicationNumber");
                                                                                                                    if (
                                                                                                                                                                        "xxxxx".equals(tarId)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "1".equals(tarCode)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "1".equals(country)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "1".equals(tarRegion)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "1".equals(tarName)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "1".equals(tarSign)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "1".equals(tarType)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "1".equals(tarGradient)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "1".equals(tarReliability)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "1".equals(coordinateSystem)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "1".equals(subPosition)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "1".equals(position)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "1".equals(lon)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "1".equals(lat)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "1".equals(geoHeight)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "1".equals(altitude)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "1".equals(area)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "1".equals(remark)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "1".equals(pointError)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "1".equals(lonError)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "1".equals(latError)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "1".equals(geoHeightError)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "1".equals(altitudeError)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "1".equals(masterMapError)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "1".equals(tarMapError)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "1".equals(plotError)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "1".equals(resultClassification)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "1".equals(reorganizeUnit)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "1".equals(reorganizeDate)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "1".equals(publicationNumber)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String upNodeFast(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取类型
                    String type = jsonObject.getString("type");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String subAttributeEditFast(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String subId = jsonObject.getString("subId");
                                                                                //jsonobject解析获取子MB标识
                    String subSign = jsonObject.getString("subSign");
                                                                                //jsonobject解析获取名称
                    String subName = jsonObject.getString("subName");
                                                                                //jsonobject解析获取标识码
                    String subCode = jsonObject.getString("subCode");
                                                                                //jsonobject解析获取类型
                    String subType = jsonObject.getString("subType");
                                                                                //jsonobject解析获取材质类型
                    String materialType = jsonObject.getString("materialType");
                                                                                //jsonobject解析获取结构类型
                    String structureType = jsonObject.getString("structureType");
                                                                                //jsonobject解析获取长度（米）
                    String length = jsonObject.getString("length");
                                                                                //jsonobject解析获取宽度（米）
                    String wide = jsonObject.getString("wide");
                                                                                //jsonobject解析获取高度（米）
                    String height = jsonObject.getString("height");
                                                                                //jsonobject解析获取方位角（度）
                    String azimuthAngle = jsonObject.getString("azimuthAngle");
                                                                                //jsonobject解析获取圆内半径（米）
                    String inRadius = jsonObject.getString("inRadius");
                                                                                //jsonobject解析获取圆外半径（米）
                    String outRadius = jsonObject.getString("outRadius");
                                                                                //jsonobject解析获取坐标系
                    String coordinateSystem = jsonObject.getString("coordinateSystem");
                                                                                //jsonobject解析获取定位点
                    String position = jsonObject.getString("position");
                                                                                //jsonobject解析获取经度（度分秒）
                    String lon = jsonObject.getString("lon");
                                                                                //jsonobject解析获取纬度（度分秒）
                    String lat = jsonObject.getString("lat");
                                                                                //jsonobject解析获取大地高度（米）
                    String geoHeight = jsonObject.getString("geoHeight");
                                                                                //jsonobject解析获取海拔高度（米）
                    String altitude = jsonObject.getString("altitude");
                                                                                //jsonobject解析获取面积（平方米）
                    String area = jsonObject.getString("area");
                                                                                //jsonobject解析获取备注
                    String remark = jsonObject.getString("remark");
                                                                                //jsonobject解析获取点位误差（米）
                    String pointError = jsonObject.getString("pointError");
                                                                                //jsonobject解析获取经度误差（秒）
                    String lonError = jsonObject.getString("lonError");
                                                                                //jsonobject解析获取纬度误差（秒）
                    String latError = jsonObject.getString("latError");
                                                                                //jsonobject解析获取大地高误差（米）
                    String geoHeightError = jsonObject.getString("geoHeightError");
                                                                                //jsonobject解析获取海拔高误差（米）
                    String altitudeError = jsonObject.getString("altitudeError");
                                                                                //jsonobject解析获取原图误差（米）
                    String masterMapError = jsonObject.getString("masterMapError");
                                                                                //jsonobject解析获取图制作误差（米）
                    String tarMapError = jsonObject.getString("tarMapError");
                                                                                //jsonobject解析获取地物加绘误差（米）
                    String plotError = jsonObject.getString("plotError");
                                                                                                                    if (
                                                                                                                                                                        "xxxx".equals(subId)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "1".equals(subSign)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "1".equals(subName)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "1".equals(subCode)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "1".equals(subType)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "1".equals(materialType)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "1".equals(structureType)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "1".equals(length)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "1".equals(wide)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "1".equals(height)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "1".equals(azimuthAngle)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "1".equals(inRadius)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "1".equals(outRadius)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "1".equals(coordinateSystem)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "1".equals(position)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "1".equals(lon)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "1".equals(lat)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "1".equals(geoHeight)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "1".equals(altitude)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "1".equals(area)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "1".equals(remark)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "1".equals(pointError)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "1".equals(lonError)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "1".equals(latError)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "1".equals(geoHeightError)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "1".equals(altitudeError)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "1".equals(masterMapError)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "1".equals(tarMapError)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "1".equals(plotError)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String tarAttributeFast(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String tarId = jsonObject.getString("tarId");
                                                                                                                    if (
                                                                                                                                                                        "123311222222222222222".equals(tarId)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{\"country\":\"1\",\"altitude\":\"1\",\"masterMapError\":\"1\",\"tarSign\":\"MB标识\",\"latError\":\"1\",\"geoHeight\":\"1\",\"lon\":\"1\",\"remark\":\"1\",\"altitudeError\":\"1\",\"tarCode\":\"1\",\"plotError\":\"1\",\"pointError\":\"1\",\"tarName\":\"1\",\"tarMapError\":\"1\",\"tarGradient\":\"1\",\"resultClassification\":\"1\",\"coordinateSystem\":\"1\",\"lat\":\"1\",\"reorganizeUnit\":\"1\",\"publicationNumber\":\"1\",\"area\":\"1\",\"subPosition\":\"1\",\"tarReliability\":\"1\",\"lonError\":\"1\",\"geoHeightError\":\"1\",\"tarSerial\":\"1\",\"tarType\":\"1\",\"reorganizeDate\":\"1\",\"position\":\"1\",\"tarRegion\":\"1\"},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String subAttributeFast(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String subId = jsonObject.getString("subId");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{\"altitude\":\"1\",\"subSign\":\"1\",\"masterMapError\":\"1\",\"latError\":\"1\",\"geoHeight\":\"1\",\"lon\":\"1\",\"remark\":\"1\",\"altitudeError\":\"1\",\"azimuthAngle\":\"1\",\"plotError\":\"1\",\"pointError\":\"1\",\"tarMapError\":\"1\",\"subCode\":\"1\",\"outRadius\":\"1\",\"coordinateSystem\":\"1\",\"lat\":\"1\",\"height\":\"1\",\"area\":\"1\",\"wide\":\"1\",\"materialType\":\"1\",\"length\":\"1\",\"inRadius\":\"1\",\"lonError\":\"1\",\"subName\":\"1\",\"structureType\":\"1\",\"geoHeightError\":\"1\",\"subType\":\"1\",\"position\":\"1\"},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
    
}
