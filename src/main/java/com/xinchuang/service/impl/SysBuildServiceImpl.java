package com.xinchuang.service.impl;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xinchuang.comment.R;
import com.xinchuang.service.SysBuildService;
import com.xinchuang.utils.CheckUtil;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.text.SimpleDateFormat;
import com.xinchuang.utils.DateUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSON;

@Service
public class SysBuildServiceImpl implements SysBuildService {

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                                        @Override
                public String sysList(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取关键字
                    String keyword = jsonObject.getString("keyword");
                                                                                //jsonobject解析获取国家列表
                    JSONArray country = jsonObject.getJSONArray("country");
                                                                                //jsonobject解析获取分类列表
                    JSONArray classify = jsonObject.getJSONArray("classify");
                                                                                                                    if (
                                                                                                                                                                        "{}".equals(JSON.toJSONString(country))
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "{}".equals(JSON.toJSONString(classify))
                                                                                                                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String sysInfo(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String sId = jsonObject.getString("sId");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String sysDelete(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取系统标识
                    String sId = jsonObject.getString("sId");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String sysInfoEdit() {
                                                                                            if (1 ==1
                                            ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String relationEdit(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取系统
                    String sId = jsonObject.getString("sId");
                                                                                //jsonobject解析获取MB1标识
                    String tar1Id = jsonObject.getString("tar1Id");
                                                                                //jsonobject解析获取关系列表
                    JSONArray relList = jsonObject.getJSONArray("relList");
                                                                                                                    if (
                                                                                                                                                                        "[{}]".equals(JSON.toJSONString(relList))
                                                                                                                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String mbWaitList() {
                                                                                            if (1 ==1
                                            ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
    
}
