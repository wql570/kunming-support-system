package com.xinchuang.service.impl;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xinchuang.comment.R;
import com.xinchuang.service.PushService;
import com.xinchuang.utils.CheckUtil;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.text.SimpleDateFormat;
import com.xinchuang.utils.DateUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSON;

@Service
public class PushServiceImpl implements PushService {

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                                        @Override
                public String reportList(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取页码
                    String pageNum = jsonObject.getString("pageNum");
                                                                                //jsonobject解析获取页容
                    String pageSize = jsonObject.getString("pageSize");
                                                                                //jsonobject解析获取名称
                    String fileName = jsonObject.getString("fileName");
                                                                                //jsonobject解析获取类型
                    String type = jsonObject.getString("type");
                                                                                                                    if (
                                                                                                                                                                        "1".equals(pageNum)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "10".equals(pageSize)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "文件名称".equals(fileName)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "0".equals(type)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{\"fileList\":[{}]},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String serveEdit(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取地址
                    String ip = jsonObject.getString("ip");
                                                                                //jsonobject解析获取端口
                    String port = jsonObject.getString("port");
                                                                                //jsonobject解析获取备注
                    String remark = jsonObject.getString("remark");
                                                                                                                    if (
                                                                                                                                                                        "192.168.1.200".equals(ip)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "8000".equals(port)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "备注信息".equals(remark)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String serveUpload(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识列表
                    JSONArray ids = jsonObject.getJSONArray("ids");
                                                                                                                    if (
                                                                                                                                                                        "[\"xxxx\",\"cccc\"]".equals(JSON.toJSONString(ids))
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{},\"success\":true}";
                                                    }
                                                                                        if (
                                                                                                                                                                        "{}".equals(JSON.toJSONString(ids))
                                                                                                                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String reportUpload(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取类型
                    String type = jsonObject.getString("type");
                                                                                //jsonobject解析获取名称
                    String fileName = jsonObject.getString("fileName");
                                                                                //jsonobject解析获取文件地址
                    String fileUrl = jsonObject.getString("fileUrl");
                                                                                                                    if (
                                                                                                                                                                        "0".equals(type)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "文件名称".equals(fileName)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "地址".equals(fileUrl)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String reportDownload(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取类型
                    String type = jsonObject.getString("type");
                                                                                //jsonobject解析获取标识列表
                    JSONArray ids = jsonObject.getJSONArray("ids");
                                                                                                                    if (
                                                                                                                                                                        "0".equals(type)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "[\"xxx\",\"ccc\"]".equals(JSON.toJSONString(ids))
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{},\"success\":true}";
                                                    }
                                                                                        if (
                                                                                                                                                                        "{}".equals(JSON.toJSONString(ids))
                                                                                                                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String reportDelete(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取类型
                    String type = jsonObject.getString("type");
                                                                                //jsonobject解析获取标识数组
                    JSONArray ids = jsonObject.getJSONArray("ids");
                                                                                                                    if (
                                                                                                                                                                        "0".equals(type)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "[\"ssss\",\"aaaa\"]".equals(JSON.toJSONString(ids))
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{},\"success\":true}";
                                                    }
                                                                                        if (
                                                                                                                                                                        "{}".equals(JSON.toJSONString(ids))
                                                                                                                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
    
}
