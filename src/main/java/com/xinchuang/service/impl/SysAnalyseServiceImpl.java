package com.xinchuang.service.impl;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xinchuang.comment.R;
import com.xinchuang.service.SysAnalyseService;
import com.xinchuang.utils.CheckUtil;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.text.SimpleDateFormat;
import com.xinchuang.utils.DateUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSON;

@Service
public class SysAnalyseServiceImpl implements SysAnalyseService {

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                                        @Override
                public String sysList(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取页码
                    String pageNum = jsonObject.getString("pageNum");
                                                                                //jsonobject解析获取页容
                    String pageSize = jsonObject.getString("pageSize");
                                                                                //jsonobject解析获取关键字
                    String keyword = jsonObject.getString("keyword");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String mbPlotInfo(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取系统标识
                    String sId = jsonObject.getString("sId");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String subList(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取系统标识
                    String sId = jsonObject.getString("sId");
                                                                                //jsonobject解析获取MB标识
                    String tarId = jsonObject.getString("tarId");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String mbList(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取系统标识
                    String sId = jsonObject.getString("sId");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String sysInfo(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取系统标识
                    String sId = jsonObject.getString("sId");
                                                                                //jsonobject解析获取类型
                    String type = jsonObject.getString("type");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String analyseImportant() {
                                                                                            if (1 ==1
                                            ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String planeReach(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取系统标识
                    String sId = jsonObject.getString("sId");
                                                                                //jsonobject解析获取MB标识
                    String tarId = jsonObject.getString("tarId");
                                                                                //jsonobject解析获取机型
                    String airId = jsonObject.getString("airId");
                                                                                //jsonobject解析获取加油次数
                    String number = jsonObject.getString("number");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String lineAnalyse(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取系统标识
                    String sId = jsonObject.getString("sId");
                                                                                //jsonobject解析获取MB标识集合
                    JSONArray tarIds = jsonObject.getJSONArray("tarIds");
                                                                                //jsonobject解析获取机型
                    String airId = jsonObject.getString("airId");
                                                                                                                    if (
                                                                                                                                                                        "{}".equals(JSON.toJSONString(tarIds))
                                                                                                                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String planeUsable(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取系统标识
                    String sId = jsonObject.getString("sId");
                                                                                //jsonobject解析获取MB标识集合
                    JSONArray tarIds = jsonObject.getJSONArray("tarIds");
                                                                                //jsonobject解析获取加油次数
                    String number = jsonObject.getString("number");
                                                                                                                    if (
                                                                                                                                                                        "{}".equals(JSON.toJSONString(tarIds))
                                                                                                                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String analyseReportExport(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取系统标识
                    String sId = jsonObject.getString("sId");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
    
}
