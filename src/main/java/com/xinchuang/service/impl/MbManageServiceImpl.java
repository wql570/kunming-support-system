package com.xinchuang.service.impl;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xinchuang.comment.R;
import com.xinchuang.service.MbManageService;
import com.xinchuang.utils.CheckUtil;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.text.SimpleDateFormat;
import com.xinchuang.utils.DateUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSON;

@Service
public class MbManageServiceImpl implements MbManageService {

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                                        @Override
                public String mbList(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取页码
                    String pageNum = jsonObject.getString("pageNum");
                                                                                //jsonobject解析获取页容
                    String pageSize = jsonObject.getString("pageSize");
                                                                                //jsonobject解析获取关键字
                    String keyword = jsonObject.getString("keyword");
                                                                                //jsonobject解析获取国家列表
                    JSONArray country = jsonObject.getJSONArray("country");
                                                                                //jsonobject解析获取分类列表
                    JSONArray classify = jsonObject.getJSONArray("classify");
                                                                                                                    if (
                                                                                                                                                                        "{}".equals(JSON.toJSONString(country))
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "{}".equals(JSON.toJSONString(classify))
                                                                                                                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String subList(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取MB标识
                    String targetId = jsonObject.getString("targetId");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String mbInfo(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取MB标识
                    String tarId = jsonObject.getString("tarId");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String subInfo(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取MB标识
                    String tarId = jsonObject.getString("tarId");
                                                                                //jsonobject解析获取子MB标识
                    String subId = jsonObject.getString("subId");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String mbDetail(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取MB标识
                    String tarId = jsonObject.getString("tarId");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String subDetail(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取MB标识
                    String tarId = jsonObject.getString("tarId");
                                                                                //jsonobject解析获取子MB标识
                    String subId = jsonObject.getString("subId");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String mbInfoEdit(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取MB标识
                    String tarId = jsonObject.getString("tarId");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String subInfoEdit(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取MB标识
                    String tarId = jsonObject.getString("tarId");
                                                                                //jsonobject解析获取子MB标识
                    String subId = jsonObject.getString("subId");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String mbDetailEdit(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取MB标识
                    String tarId = jsonObject.getString("tarId");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String subDetailEdit(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取子MB标识
                    String subId = jsonObject.getString("subId");
                                                                                //jsonobject解析获取MB标识
                    String tarId = jsonObject.getString("tarId");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String mbDelete(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取MB标识
                    JSONArray tarIds = jsonObject.getJSONArray("tarIds");
                                                                                                                    if (
                                                                                                                                                                        "{}".equals(JSON.toJSONString(tarIds))
                                                                                                                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String subDelete(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取MB标识
                    String tarId = jsonObject.getString("tarId");
                                                                                //jsonobject解析获取子MB标识
                    JSONArray subIds = jsonObject.getJSONArray("subIds");
                                                                                                                    if (
                                                                                                                                                                        "{}".equals(JSON.toJSONString(subIds))
                                                                                                                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
    
}
