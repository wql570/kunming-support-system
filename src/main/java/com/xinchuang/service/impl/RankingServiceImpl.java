package com.xinchuang.service.impl;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xinchuang.comment.R;
import com.xinchuang.service.RankingService;
import com.xinchuang.utils.CheckUtil;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.text.SimpleDateFormat;
import com.xinchuang.utils.DateUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSON;

@Service
public class RankingServiceImpl implements RankingService {

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                                        @Override
                public String rankingList(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取页码
                    String pageNum = jsonObject.getString("pageNum");
                                                                                //jsonobject解析获取页容
                    String pageSize = jsonObject.getString("pageSize");
                                                                                //jsonobject解析获取日期
                    JSONArray date = jsonObject.getJSONArray("date");
                                                                                //jsonobject解析获取排序
                    String sortord = jsonObject.getString("sortord");
                                                                                                                    if (
                                                                                                                                                                        "1".equals(pageNum)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "10".equals(pageSize)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "[\"2023-1-1\",\"2023-1-10\"]".equals(JSON.toJSONString(date))
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "0".equals(sortord)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{\"total\":\"100\",\"pangeSize\":\"10\",\"rankingList\":[{\"lastTime\":\"2023-1-9\",\"number\":\"12\",\"accuracy\":\"0.9\",\"xName\":\"xxxx\"}],\"pageAll\":\"10\",\"pageNum\":\"1\"},\"success\":true}";
                                                    }
                                                                                        if (
                                                                                                                                                                        "{}".equals(JSON.toJSONString(date))
                                                                                                                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
    
}
