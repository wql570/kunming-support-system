package com.xinchuang.service.impl;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xinchuang.comment.R;
import com.xinchuang.service.DictionariesService;
import com.xinchuang.utils.CheckUtil;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.text.SimpleDateFormat;
import com.xinchuang.utils.DateUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSON;

@Service
public class DictionariesServiceImpl implements DictionariesService {

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                                        @Override
                public String attributeList(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取页码
                    String pageNum = jsonObject.getString("pageNum");
                                                                                //jsonobject解析获取页容
                    String pangeSize = jsonObject.getString("pangeSize");
                                                                                //jsonobject解析获取类型
                    String type = jsonObject.getString("type");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String attributeEdit(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取类型
                    String type = jsonObject.getString("type");
                                                                                //jsonobject解析获取标识
                    String id = jsonObject.getString("id");
                                                                                //jsonobject解析获取名称
                    String keyName = jsonObject.getString("keyName");
                                                                                //jsonobject解析获取字段
                    String key = jsonObject.getString("key");
                                                                                //jsonobject解析获取类型
                    String keyType = jsonObject.getString("keyType");
                                                                                //jsonobject解析获取显示类型
                    String showType = jsonObject.getString("showType");
                                                                                //jsonobject解析获取文本域行数
                    String row = jsonObject.getString("row");
                                                                                //jsonobject解析获取必填
                    String required = jsonObject.getString("required");
                                                                                //jsonobject解析获取提示信息
                    String message = jsonObject.getString("message");
                                                                                //jsonobject解析获取模板信息
                    String description = jsonObject.getString("description");
                                                                                //jsonobject解析获取最大最小值
                    String minmax = jsonObject.getString("minmax");
                                                                                //jsonobject解析获取单位
                    String unit = jsonObject.getString("unit");
                                                                                //jsonobject解析获取是否统计
                    String statis = jsonObject.getString("statis");
                                                                                //jsonobject解析获取枚举值列表
                    JSONArray enumerator = jsonObject.getJSONArray("enumerator");
                                                                                                                    if (
                                                                                                                                                                        "[{}]".equals(JSON.toJSONString(enumerator))
                                                                                                                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String attributeDelete(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取类型
                    String type = jsonObject.getString("type");
                                                                                //jsonobject解析获取标识
                    JSONArray id = jsonObject.getJSONArray("id");
                                                                                                                    if (
                                                                                                                                                                        "{}".equals(JSON.toJSONString(id))
                                                                                                                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String typeList() {
                                                                                            if (1 ==1
                                            ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String typeEdit() {
                                                                                            if (1 ==1
                                            ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String typeDelete() {
                                                                                            if (1 ==1
                                            ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String typeAttributeList(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取页容
                    String pangeSize = jsonObject.getString("pangeSize");
                                                                                //jsonobject解析获取页码
                    String pageNum = jsonObject.getString("pageNum");
                                                                                //jsonobject解析获取标识
                    String typeId = jsonObject.getString("typeId");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String typeAttributeEdit(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取字段
                    String key = jsonObject.getString("key");
                                                                                //jsonobject解析获取名称
                    String keyName = jsonObject.getString("keyName");
                                                                                //jsonobject解析获取标识
                    String typeId = jsonObject.getString("typeId");
                                                                                //jsonobject解析获取类型
                    String keyType = jsonObject.getString("keyType");
                                                                                //jsonobject解析获取显示类型
                    String showType = jsonObject.getString("showType");
                                                                                //jsonobject解析获取文本域行数
                    String row = jsonObject.getString("row");
                                                                                //jsonobject解析获取必填
                    String required = jsonObject.getString("required");
                                                                                //jsonobject解析获取提示信息
                    String message = jsonObject.getString("message");
                                                                                //jsonobject解析获取模板信息
                    String description = jsonObject.getString("description");
                                                                                //jsonobject解析获取最大最小值
                    String minmax = jsonObject.getString("minmax");
                                                                                //jsonobject解析获取单位
                    String unit = jsonObject.getString("unit");
                                                                                //jsonobject解析获取是否统计
                    String statis = jsonObject.getString("statis");
                                                                                //jsonobject解析获取枚举值列表
                    JSONArray enumerator = jsonObject.getJSONArray("enumerator");
                                                                                                                    if (
                                                                                                                                                                        "[{}]".equals(JSON.toJSONString(enumerator))
                                                                                                                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String typeAttributeDelete(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String typeId = jsonObject.getString("typeId");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String knowList(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取页码
                    String pageNum = jsonObject.getString("pageNum");
                                                                                //jsonobject解析获取页容
                    String pageSize = jsonObject.getString("pageSize");
                                                                                //jsonobject解析获取标识
                    String typeId = jsonObject.getString("typeId");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String knowEdit(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取知识描述
                    String describe = jsonObject.getString("describe");
                                                                                //jsonobject解析获取类型
                    String type = jsonObject.getString("type");
                                                                                //jsonobject解析获取类型标识
                    String typeId = jsonObject.getString("typeId");
                                                                                //jsonobject解析获取指标选择
                    String index = jsonObject.getString("index");
                                                                                //jsonobject解析获取标识
                    String id = jsonObject.getString("id");
                                                                                //jsonobject解析获取知识种类
                    String kindName = jsonObject.getString("kindName");
                                                                                //jsonobject解析获取知识名称
                    String knowName = jsonObject.getString("knowName");
                                                                                //jsonobject解析获取知识类型
                    String knowType = jsonObject.getString("knowType");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String knowDelete(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取类型标识
                    String typeId = jsonObject.getString("typeId");
                                                                                //jsonobject解析获取标识
                    String id = jsonObject.getString("id");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String upDownSpec(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String typeId = jsonObject.getString("typeId");
                                                                                //jsonobject解析获取移动
                    String sort = jsonObject.getString("sort");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String upDown(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取移动
                    String sort = jsonObject.getString("sort");
                                                                                //jsonobject解析获取标识
                    String id = jsonObject.getString("id");
                                                                                //jsonobject解析获取类别
                    String type = jsonObject.getString("type");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
    
}
