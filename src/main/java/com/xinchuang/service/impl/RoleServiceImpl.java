package com.xinchuang.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.xinchuang.comment.R;
import com.xinchuang.service.RoleService;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;

@Service
public class RoleServiceImpl implements RoleService {

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public String roleList() {
        if (1 == 1
                ) {
            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":[{\"role\":\"管理员\",\"remark\":\"系统调度\",\"id\":\"0\",\"url\":\"https://t7.baidu.com/it/u=508006830,4042443322&fm=193&f=GIF\"},{\"role\":\"业务员\",\"remark\":\"业务处理\",\"id\":\"1\",\"url\":\"https://t7.baidu.com/it/u=508006830,4042443322&fm=193&f=GIF\"}],\"success\":true}";
        }
        if (1 == 1
                ) {
            return "";
        }
        return JSON.toJSONString(R.success("无数据"));
    }

    @Override
    public String navList(JSONObject jsonObject) {
        //jsonobject解析获取类型
        String role = jsonObject.getString("role");
        if (
                "0".equals(role)
                ) {
            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":[{\"subSysEName\":\"Object recognition System\",\"subSysName\":\"MB智能识别子系统\",\"accredit\":\"ture\",\"id\":\"0\"},{\"subSysEName\":\"Object detail management System\",\"subSysName\":\"MB特征展示子系统\",\"id\":\"1\",\"accredit\":\"true\"},{\"subSysEName\":\"System analysis System\",\"subSysName\":\"体系分析子系统\",\"accredit\":\"true\",\"id\":\"2\"},{\"subSysEName\":\"Syetem management System\",\"subSysName\":\"系统管理子系统\",\"accredit\":\"true\",\"id\":\"3\"}],\"success\":true}";
        }
        if (
                "2".equals(role)
                ) {
            return "";
        }
        if (
                "1".equals(role)
                ) {
            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":[{\"subSysEName\":\"System analysis System\",\"subSysName\":\"体系分析子系统\",\"accredit\":\"true\",\"id\":\"2\"},{\"subSysEName\":\"Object detail management System\",\"subSysName\":\"MB特征展示子系统\",\"id\":\"1\",\"accredit\":\"true\"},{\"subSysEName\":\"Syetem management System\",\"subSysName\":\"系统管理子系统\",\"accredit\":\"false\",\"id\":\"3\"},{\"subSysEName\":\"Object recognition System\",\"subSysName\":\"MB智能识别子系统\",\"id\":\"0\",\"accredit\":\"ture\"}],\"success\":true}";
        }
        return JSON.toJSONString(R.success("无数据"));
    }

    @Override
    public String roleAccredit(JSONObject jsonObject) {
        //jsonobject解析获取角色
        String role = jsonObject.getString("role");
        //jsonobject解析获取子系统标识
        String subSysName = jsonObject.getString("subSysName");
        //jsonobject解析获取授权
        String accredit = jsonObject.getString("accredit");
        if (
                "0".equals(role)
                        &&
                        "智能识别子系统".equals(subSysName)
                        &&
                        "true".equals(accredit)
                ) {
            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{},\"success\":true}";
        }
        if (jsonObject == null || jsonObject.size() < 1
                ) {
            return "";
        }
        if (
                "智能识别子系统".equals(subSysName)
                        &&
                        "true".equals(accredit)
                        &&
                        "1".equals(role)
                ) {
            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{},\"success\":true}";
        }
        return JSON.toJSONString(R.success("无数据"));
    }

    @Override
    public String accreditAll(JSONObject jsonObject) {
        //jsonobject解析获取授权所有
        String accreditAll = jsonObject.getString("accreditAll");
        //jsonobject解析获取类型
        String role = jsonObject.getString("role");
        if (
                "true".equals(accreditAll)
                        &&
                        "0".equals(role)
                ) {
            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{},\"success\":true}";
        }
        if (jsonObject == null || jsonObject.size() < 1
                ) {
            return "";
        }
        if (
                "true".equals(accreditAll)
                        &&
                        "1".equals(role)
                ) {
            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{},\"success\":true}";
        }
        return JSON.toJSONString(R.success("无数据"));
    }

}
