package com.xinchuang.service.impl;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xinchuang.comment.R;
import com.xinchuang.service.PhotoService;
import com.xinchuang.utils.CheckUtil;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.text.SimpleDateFormat;
import com.xinchuang.utils.DateUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSON;

@Service
public class PhotoServiceImpl implements PhotoService {

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                                        @Override
                public String imgList(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取页码
                    String pageNum = jsonObject.getString("pageNum");
                                                                                //jsonobject解析获取页容
                    String pageSize = jsonObject.getString("pageSize");
                                                                                //jsonobject解析获取图片名称
                    String imgName = jsonObject.getString("imgName");
                                                                                                                    if (
                                                                                                                                                                        "1".equals(pageNum)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "10".equals(pageSize)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "名称".equals(imgName)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{\"total\":\"43\",\"pageAll\":\"2\",\"pageSize\":\"10\",\"pageNum\":\"1\",\"imgList\":[{\"imgUrl2\":\"ss\",\"imgUrl1\":\"ssssssss\",\"updateTime\":\"qq\",\"imgName2\":\"图片2\",\"id\":\"xxxxxxxx\",\"userName\":\"qq\",\"imgName1\":\"图片1\"}]},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String imgUpload(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取图片地址1
                    String imgUrl1 = jsonObject.getString("imgUrl1");
                                                                                //jsonobject解析获取图片地址2
                    String imgUrl2 = jsonObject.getString("imgUrl2");
                                                                                //jsonobject解析获取名称1
                    String imgName1 = jsonObject.getString("imgName1");
                                                                                //jsonobject解析获取名称2
                    String imgName2 = jsonObject.getString("imgName2");
                                                                                //jsonobject解析获取标绘1
                    String plotList1 = jsonObject.getString("plotList1");
                                                                                //jsonobject解析获取标绘2
                    String plotList2 = jsonObject.getString("plotList2");
                                                                                //jsonobject解析获取标识
                    String id = jsonObject.getString("id");
                                                                                                                    if (
                                                                                                                                                                        "qqq".equals(imgUrl1)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "aaaa".equals(imgUrl2)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "名称1".equals(imgName1)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "名称2".equals(imgName2)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{\"id\":\"xxxxxxxxxx\"},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String plotEdit(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String id = jsonObject.getString("id");
                                                                                //jsonobject解析获取图片1
                    String img1 = jsonObject.getString("img1");
                                                                                //jsonobject解析获取图片2
                    String img2 = jsonObject.getString("img2");
                                                                                                                    if (
                                                                                                                                                                        "1".equals(id)
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{},\"success\":true}";
                                                    }
                                                                                        if (
                                                                                                                                                                        "".equals(img1)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "".equals(img2)
                                                                                                                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String imgDelete(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    JSONArray ids = jsonObject.getJSONArray("ids");
                                                                                                                    if (
                                                                                                                                                                        "[\"xxxxxxxxx\"]".equals(JSON.toJSONString(ids))
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{},\"success\":true}";
                                                    }
                                                                                        if (
                                                                                                                                                                        "{}".equals(JSON.toJSONString(ids))
                                                                                                                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
    
}
