package com.xinchuang.service.impl;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xinchuang.comment.R;
import com.xinchuang.service.RasterService;
import com.xinchuang.utils.CheckUtil;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.text.SimpleDateFormat;
import com.xinchuang.utils.DateUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSON;

@Service
public class RasterServiceImpl implements RasterService {

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                                        @Override
                public String imgList(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String tarId = jsonObject.getString("tarId");
                                                                                //jsonobject解析获取页码
                    String pageNum = jsonObject.getString("pageNum");
                                                                                //jsonobject解析获取页容
                    String pageSize = jsonObject.getString("pageSize");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{\"imgList\":[{\"thuUrl\":\"https://t7.baidu.com/it/u=508006830,4042443322&fm=193&f=GIF\"}]},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String img(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String tarId = jsonObject.getString("tarId");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String autoRaster(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String tarId = jsonObject.getString("tarId");
                                                                                //jsonobject解析获取标绘
                    String plotlist = jsonObject.getString("plotlist");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String editPlotList(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String tarId = jsonObject.getString("tarId");
                                                                                //jsonobject解析获取标绘信息
                    String plotStr = jsonObject.getString("plotStr");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
    
}
