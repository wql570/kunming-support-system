package com.xinchuang.service.impl;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xinchuang.comment.R;
import com.xinchuang.service.ValueAnalyseService;
import com.xinchuang.utils.CheckUtil;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.text.SimpleDateFormat;
import com.xinchuang.utils.DateUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSON;

@Service
public class ValueAnalyseServiceImpl implements ValueAnalyseService {

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                                        @Override
                public String mbValueList(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取系统标识
                    String sId = jsonObject.getString("sId");
                                                                                //jsonobject解析获取类型
                    String type = jsonObject.getString("type");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String subValueList(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取类型
                    String type = jsonObject.getString("type");
                                                                                //jsonobject解析获取系统标识
                    String sId = jsonObject.getString("sId");
                                                                                //jsonobject解析获取MB标识
                    String tarId = jsonObject.getString("tarId");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String mbValueEdit(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取类型
                    String type = jsonObject.getString("type");
                                                                                //jsonobject解析获取系统标识
                    String sId = jsonObject.getString("sId");
                                                                                //jsonobject解析获取MB标识
                    String tarId = jsonObject.getString("tarId");
                                                                                //jsonobject解析获取权重
                    String tarWeight = jsonObject.getString("tarWeight");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String subValueEdit(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取类型
                    String type = jsonObject.getString("type");
                                                                                //jsonobject解析获取系统标识
                    String sId = jsonObject.getString("sId");
                                                                                //jsonobject解析获取MB标识
                    String tarId = jsonObject.getString("tarId");
                                                                                //jsonobject解析获取标识
                    String subId = jsonObject.getString("subId");
                                                                                //jsonobject解析获取权重
                    String subWeight = jsonObject.getString("subWeight");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String knoValueEdit(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取类型
                    String type = jsonObject.getString("type");
                                                                                //jsonobject解析获取系统标识
                    String sId = jsonObject.getString("sId");
                                                                                //jsonobject解析获取MB标识
                    String tarId = jsonObject.getString("tarId");
                                                                                //jsonobject解析获取子MB标识
                    String subId = jsonObject.getString("subId");
                                                                                //jsonobject解析获取标识
                    String knowId = jsonObject.getString("knowId");
                                                                                //jsonobject解析获取权重
                    String weight = jsonObject.getString("weight");
                                                                                //jsonobject解析获取评分
                    String score = jsonObject.getString("score");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
    
}
