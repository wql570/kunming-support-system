package com.xinchuang.service.impl;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xinchuang.comment.R;
import com.xinchuang.service.ImageService;
import com.xinchuang.utils.CheckUtil;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.text.SimpleDateFormat;
import com.xinchuang.utils.DateUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSON;

@Service
public class ImageServiceImpl implements ImageService {

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                                        @Override
                public String reslutList(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取页码
                    String pageNum = jsonObject.getString("pageNum");
                                                                                //jsonobject解析获取页容
                    String pageSize = jsonObject.getString("pageSize");
                                                                                //jsonobject解析获取名称
                    String imgName = jsonObject.getString("imgName");
                                                                                //jsonobject解析获取类型
                    String type = jsonObject.getString("type");
                                                                                                                    if (
                                                                                                                                                                        "1".equals(pageNum)
                                                                                                                                                                                                                                                        &&
                                                                                                                                                                                                                        "10".equals(pageSize)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "成果名称".equals(imgName)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "0".equals(type)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{\"imgList\":[{}]},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String correctResult(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String tarId = jsonObject.getString("tarId");
                                                                                                                    if (
                                                                                                                                                                        "xxx".equals(tarId)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String recognitionResult(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String tarId = jsonObject.getString("tarId");
                                                                                                                    if (
                                                                                                                                                                        "xxx".equals(tarId)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String rasterResult(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String tarId = jsonObject.getString("tarId");
                                                                                                                    if (
                                                                                                                                                                        "xxx".equals(tarId)
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{},\"success\":true}";
                                                    }
                                                                                    if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String resultDelete(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取类型
                    String type = jsonObject.getString("type");
                                                                                //jsonobject解析获取标识
                    JSONArray ids = jsonObject.getJSONArray("ids");
                                                                                                                    if (
                                                                                                                                                                        "0".equals(type)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "[\"xxx\"]".equals(JSON.toJSONString(ids))
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{},\"success\":true}";
                                                    }
                                                                                        if (
                                                                                                                                                                        "{}".equals(JSON.toJSONString(ids))
                                                                                                                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String resultExport(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取类型
                    String type = jsonObject.getString("type");
                                                                                //jsonobject解析获取标识
                    JSONArray ids = jsonObject.getJSONArray("ids");
                                                                                                                    if (
                                                                                                                                                                        "0".equals(type)
                                                                                                                                                                                                                                                            &&
                                                                                                                                                                                                                        "[\"xxx\"]".equals(JSON.toJSONString(ids))
                                                                                                                                                                    ){
                                                            return "{\"msg\":\"操作成功\",\"code\":200,\"data\":{},\"success\":true}";
                                                    }
                                                                                        if (
                                                                                                                                                                        "{}".equals(JSON.toJSONString(ids))
                                                                                                                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
    
}
