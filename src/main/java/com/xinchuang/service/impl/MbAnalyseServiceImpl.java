package com.xinchuang.service.impl;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xinchuang.comment.R;
import com.xinchuang.service.MbAnalyseService;
import com.xinchuang.utils.CheckUtil;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.text.SimpleDateFormat;
import com.xinchuang.utils.DateUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSON;

@Service
public class MbAnalyseServiceImpl implements MbAnalyseService {

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                                        @Override
                public String mbInfo(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String tarId = jsonObject.getString("tarId");
                                                                                //jsonobject解析获取类型
                    String type = jsonObject.getString("type");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String analyseImportant(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String id = jsonObject.getString("id");
                                                                                //jsonobject解析获取类型
                    String type = jsonObject.getString("type");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String analyseCalc(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String tarId = jsonObject.getString("tarId");
                                                                                //jsonobject解析获取机型数组
                    JSONArray airIds = jsonObject.getJSONArray("airIds");
                                                                                                                    if (
                                                                                                                                                                        "{}".equals(JSON.toJSONString(airIds))
                                                                                                                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String analyseReportExport(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String tarId = jsonObject.getString("tarId");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String printscreen(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取标识
                    String tarId = jsonObject.getString("tarId");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
                                        @Override
                public String plotSave(JSONObject jsonObject) {
                                                                                    //jsonobject解析获取MB标识
                    String tarId = jsonObject.getString("tarId");
                                                                                //jsonobject解析获取标绘信息
                    String plotInfo = jsonObject.getString("plotInfo");
                                                                                                                if (jsonObject == null || jsonObject.size() < 1
                                                                    ){
                                                            return "";
                                                    }
                                return JSON.toJSONString(R.success("无数据"));
            }
    
}
