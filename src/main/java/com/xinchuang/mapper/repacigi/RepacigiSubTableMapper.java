package com.xinchuang.mapper.repacigi;

import com.xinchuang.entity.repacigi.RepacigiSubTable;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
public interface RepacigiSubTableMapper extends BaseMapper<RepacigiSubTable> {

}
