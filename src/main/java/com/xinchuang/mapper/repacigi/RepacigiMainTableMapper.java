package com.xinchuang.mapper.repacigi;

import com.xinchuang.entity.repacigi.RepacigiMainTable;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
public interface RepacigiMainTableMapper extends BaseMapper<RepacigiMainTable> {

}
