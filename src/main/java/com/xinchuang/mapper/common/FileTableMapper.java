package com.xinchuang.mapper.common;

import com.xinchuang.entity.common.FileTable;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
public interface FileTableMapper extends BaseMapper<FileTable> {

}
