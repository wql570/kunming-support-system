package com.xinchuang.mapper;

import com.xinchuang.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zx
 * @since 2023-08-29
 */
public interface UserMapper extends BaseMapper<User> {

}
