package com.xinchuang.mapper.target;

import com.xinchuang.entity.target.TargetType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
public interface TargetTypeMapper extends BaseMapper<TargetType> {

}
