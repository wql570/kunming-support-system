package com.xinchuang.mapper;

import com.xinchuang.entity.RolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zx
 * @since 2023-08-30
 */
public interface RolePermissionMapper extends BaseMapper<RolePermission> {

}
