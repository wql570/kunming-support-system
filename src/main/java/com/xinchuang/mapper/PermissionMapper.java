package com.xinchuang.mapper;

import com.xinchuang.entity.Permission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zx
 * @since 2023-08-30
 */
public interface PermissionMapper extends BaseMapper<Permission> {

}
