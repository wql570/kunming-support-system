package com.xinchuang.mapper.image;

import com.xinchuang.entity.image.ImageGrid;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
public interface ImageGridMapper extends BaseMapper<ImageGrid> {

}
