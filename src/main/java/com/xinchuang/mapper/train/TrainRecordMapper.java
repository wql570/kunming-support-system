package com.xinchuang.mapper.train;

import com.xinchuang.entity.train.TrainRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
public interface TrainRecordMapper extends BaseMapper<TrainRecord> {

}
