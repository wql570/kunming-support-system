package com.xinchuang.mapper.train;

import com.xinchuang.entity.train.TrainAnswer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
public interface TrainAnswerMapper extends BaseMapper<TrainAnswer> {

}
