package com.xinchuang.entity.image;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ImageGrid implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private String id;

    /**
     * 关联ID
     */
    private String relId;

    /**
     * 栅格名称
     */
    private String gridName;

    /**
     * 栅格形状
     */
    private String gridShape;

    /**
     * 栅格长度
     */
    private String gridLength;

    /**
     * 栅格宽度
     */
    private String gridWidth;

    /**
     * 栅格编号
     */
    private String gridNumber;

    /**
     * 标绘
     */
    private String position;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;


}
