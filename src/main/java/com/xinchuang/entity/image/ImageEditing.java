package com.xinchuang.entity.image;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ImageEditing implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private String id;

    /**
     * 关联ID
     */
    private String relId;

    /**
     * 最新ZC图片地址
     */
    private String imgUrl;

    /**
     * 控制点标绘
     */
    private String imgPosition;

    /**
     * 识别点位
     */
    private String identify;

    /**
     * 基准影像
     */
    private String referenceImg;

    /**
     * 纠正影像缩略图
     */
    private String resultThumb;

    /**
     * 纠正影像地址
     */
    private String resultImg;

    /**
     * 基准精度
     */
    private String referencePrecision;

    /**
     * 纠正误差
     */
    private String resultError;

    /**
     * 纠正精度
     */
    private String resultPrecision;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 纠正时间
     */
    private LocalDateTime resultTime;


}
