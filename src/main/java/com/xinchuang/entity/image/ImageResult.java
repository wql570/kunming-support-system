package com.xinchuang.entity.image;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ImageResult implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    /**
     * 关联ID
     */
    private String relId;

    /**
     * 成果影像名称
     */
    private String imageName;

    /**
     * 类型 1为 纠正 2为识别 3为栅格
     */
    private String imageTyoe;

    /**
     * 影像地址
     */
    private String imageUrl;

    /**
     * 影像缩略图
     */
    private String imageThumb;

    /**
     * 创建人
     */
    private String createPersion;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;


}
