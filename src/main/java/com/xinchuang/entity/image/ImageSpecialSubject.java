package com.xinchuang.entity.image;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ImageSpecialSubject implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private String id;

    /**
     * 关联ID
     */
    private String relId;

    /**
     * 专题名称
     */
    private String specialName;

    /**
     * 板式
     */
    private String plateType;

    /**
     * 比例尺
     */
    private String scale;

    /**
     * 概述说明
     */
    private String overview;

    /**
     * 经纬网格
     */
    private String graticule;

    /**
     * 图片地址
     */
    private String imgUrl;

    /**
     * 图片标绘
     */
    private String imgPosition;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;


}
