package com.xinchuang.entity.repacigi;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class RepacigiBaseTable implements Serializable {

    private static final long serialVersionUID = 1L;

    private String typeId;

    /**
     * 父类ID 默认0
     */
    private String parentId;

    /**
     * 类型名称
     */
    private String typeName;

    /**
     * 类型 0为类型 1为MB
     */
    private String type;

    /**
     * 层级
     */
    private String level;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;


}
