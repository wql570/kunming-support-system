package com.xinchuang.entity.repacigi;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ReportTable implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    /**
     * 关联ID
     */
    private String relId;

    /**
     * 报告名称
     */
    private String reportName;

    /**
     * 报告类型
     */
    private String reportType;

    /**
     * 创建人
     */
    private String createPersion;

    /**
     * 下载次数
     */
    private Integer downloadCount;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;


}
