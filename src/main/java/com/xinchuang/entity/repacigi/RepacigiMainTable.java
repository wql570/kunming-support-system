package com.xinchuang.entity.repacigi;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class RepacigiMainTable implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private String tarId;

    /**
     * 标识
     */
    private String tarSign;

    /**
     * 识别码
     */
    private String tarCode;

    /**
     * 国家地域
     */
    private String country;

    /**
     * 区
     */
    private String tarRegion;

    /**
     * 名称
     */
    private String tarName;

    /**
     * 编号
     */
    private String tarSerial;

    /**
     * 类别
     */
    private String tarType;

    /**
     * 等级
     */
    private String tarGradient;

    /**
     * 成果可信度
     */
    private String tarReliability;

    /**
     * 坐标系
     */
    private String coordinateSystem;

    /**
     * 定位点子MB
     */
    private String subPosition;

    /**
     * 定位点
     */
    private String position;

    /**
     * 经度
     */
    private String lon;

    /**
     * 纬度
     */
    private String lat;

    /**
     * 大地高度
     */
    private String geoHeight;

    /**
     * 海拔高度
     */
    private String altitude;

    /**
     * 面积
     */
    private String area;

    /**
     * 备注
     */
    private String remark;

    /**
     * 点位误差（米）
     */
    private String pointError;

    /**
     * 经度误差（秒）
     */
    private String lonError;

    /**
     * 纬度误差（秒）
     */
    private String latError;

    /**
     * 大地高误差（米）
     */
    private String geoHeightError;

    /**
     * 海拔高误差（米）
     */
    private String altitudeError;

    /**
     * 原图误差（米）
     */
    private String masterMapError;

    /**
     * 图制作误差（米）
     */
    private String tarMapError;

    /**
     * 地物加绘误差（米）
     */
    private String plotError;

    /**
     * 成果密级
     */
    private String resultClassification;

    /**
     * 整编单位
     */
    private String reorganizeUnit;

    /**
     * 整编日期
     */
    private String reorganizeDate;

    /**
     * 出版序号
     */
    private String publicationNumber;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;


}
