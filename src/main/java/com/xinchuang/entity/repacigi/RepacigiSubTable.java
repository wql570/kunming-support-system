package com.xinchuang.entity.repacigi;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class RepacigiSubTable implements Serializable {

    private static final long serialVersionUID = 1L;

    private String subId;

    /**
     * MD ID
     */
    private String targetId;

    /**
     * 子MB标识
     */
    private String subSign;

    /**
     * 名称
     */
    private String subName;

    /**
     * 标识码
     */
    private String subCode;

    /**
     * 类型
     */
    private String subType;

    /**
     * 材质类型
     */
    private String materialType;

    /**
     * 结构类型
     */
    private String structureType;

    /**
     * 长度（米）
     */
    private String length;

    /**
     * 宽度（米）
     */
    private String wide;

    /**
     * 高度（米）
     */
    private String height;

    /**
     * 方位角（度）
     */
    private String azimuthAngle;

    /**
     * 圆内半径（米）
     */
    private String inRadius;

    /**
     * 圆外半径（米）
     */
    private String outRadius;

    /**
     * 坐标系
     */
    private String coordinateSystem;

    /**
     * 定位点
     */
    private String position;

    /**
     * 经度（度分秒）
     */
    private String lon;

    /**
     * 纬度（度分秒）
     */
    private String lat;

    /**
     * 大地高度（米）
     */
    private String geoHeight;

    /**
     * 海拔高度（米）
     */
    private String altitude;

    /**
     * 面积（平方米）
     */
    private String area;

    /**
     * 备注
     */
    private String remark;

    /**
     * 点位误差（米）
     */
    private String pointError;

    /**
     * 经度误差（秒）
     */
    private String lonError;

    /**
     * 纬度误差（秒）
     */
    private String latError;

    /**
     * 大地高误差（米）
     */
    private String geoHeightError;

    /**
     * 海拔高误差（米）
     */
    private String altitudeError;

    /**
     * 原图误差（米）
     */
    private String masterMapError;

    /**
     * 图制作误差（米）
     */
    private String tarMapError;

    /**
     * 地物加绘误差（米）
     */
    private String plotError;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;


}
