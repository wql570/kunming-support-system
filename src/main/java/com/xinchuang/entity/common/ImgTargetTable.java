package com.xinchuang.entity.common;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ImgTargetTable implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 图片ID
     */
    private String imgId;

    /**
     * MB ID
     */
    private String tarId;

    /**
     * 图片名称
     */
    private String name;

    /**
     * 图片类别
     */
    private String type;

    /**
     * 图片模式
     */
    private String mode;

    /**
     * 图片地址（相对路径）
     */
    private String imgUrl;

    /**
     * 缩略图
     */
    private String thumbnail;

    /**
     * 经度
     */
    private String lon;

    /**
     * 纬度
     */
    private String lat;

    /**
     * 坐标
     */
    private String position;

    /**
     * 状态
     */
    private String status;

    /**
     * 图片大小
     */
    private String imgSize;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;


}
