package com.xinchuang.entity.common;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class FileTable implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 文件ID
     */
    private String fileId;

    /**
     * 关联ID
     */
    private String relId;

    /**
     * 文件名称
     */
    private String name;

    /**
     * 文件类别
     */
    private String type;

    /**
     * 文件地址（相对路径）
     */
    private String fileUrl;

    /**
     * 状态
     */
    private String status;

    /**
     * 文件大小
     */
    private String fileSize;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;


}
