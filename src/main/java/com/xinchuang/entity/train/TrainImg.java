package com.xinchuang.entity.train;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TrainImg implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private String id;

    /**
     * 训练图片1名称
     */
    private String imgNameOne;

    /**
     * 训练图片1
     */
    private String imgUrlOne;

    /**
     * 训练图片1标会
     */
    private String imgPositionOne;

    /**
     * 训练图片2名称
     */
    private String imgNameTwo;

    /**
     * 训练图片2
     */
    private String imgUrlTwo;

    /**
     * 训练图片2标会
     */
    private String imgPositionTwo;

    /**
     * 图片不同的数量
     */
    private Integer differentNum;

    /**
     * 创建人
     */
    private String createPerson;

    /**
     * 序号
     */
    private Integer sort;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;


}
