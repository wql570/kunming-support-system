package com.xinchuang.entity.target;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TargetSpecialAttribute implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private String id;

    /**
     * 目标类型
     */
    private String typeId;

    /**
     * 属性名称
     */
    private String keyName;

    /**
     * 字段名称
     */
    private String key;

    /**
     * 字段类型
     */
    private String keyType;

    /**
     * 显示类型
     */
    private String showType;

    /**
     * 是否必填
     */
    private String required;

    /**
     * 是否用于统计
     */
    private String info;

    /**
     * 取值范围
     */
    private String dataRange;

    /**
     * 是否校验
     */
    private String verify;

    /**
     * 最小值
     */
    private String min;

    /**
     * 最大值
     */
    private String max;

    /**
     * 枚举值
     */
    private String enumerator;

    /**
     * 文本显示类型
     */
    private String row;

    /**
     * 1为目标共有属性，2为子目标共有属性
     */
    private Integer sign;

    /**
     * 前端校验
     */
    private String message;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;


}
