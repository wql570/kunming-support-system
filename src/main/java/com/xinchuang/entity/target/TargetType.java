package com.xinchuang.entity.target;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TargetType implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 目标类型ID
     */
    private String typeId;

    /**
     * 父级ID
     */
    private String parentId;

    /**
     * 类别名称
     */
    private String typeName;

    /**
     * 树形级别
     */
    private Integer level;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;


}
