package com.xinchuang.entity.target;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TargetMainTable implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private String id;

    /**
     * MBID
     */
    private String targetId;

    /**
     * 字段ID
     */
    private String keyId;

    /**
     * 字段值
     */
    private String keyValue;

    /**
     * 备注
     */
    private String remark;

    /**
     * 编辑人
     */
    private String person;

    /**
     * 来源
     */
    private String source;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;


}
