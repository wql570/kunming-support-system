package com.xinchuang.entity.target;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author zx
 * @since 2023-08-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TargetBaseData implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * MBID
     */
    private String targetId;

    /**
     * MB名称
     */
    private String targetName;

    /**
     * MB分类
     */
    private String targetClassify;

    /**
     * 国家
     */
    private String country;

    /**
     * MB类别
     */
    private String targetType;

    /**
     * 经度
     */
    private String lon;

    /**
     * 纬度
     */
    private String lat;

    /**
     * 高程
     */
    private String height;

    /**
     * 缩略图地址（相对路径）
     */
    private String iconUrl;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 删除标记
     */
    private Integer deleteSign;


}
