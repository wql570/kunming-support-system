package com.xinchuang.utils;

import cn.hutool.core.date.DateUtil;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

/**
 * @date 2021/3/22 11:27
 * @author houerhua
 */
public class DateUtils {

    public final static String YMDHMS = "yyyy-MM-dd HH:mm:ss";

    private final static SimpleDateFormat sdfDay = new SimpleDateFormat("yyyy-MM-dd");

    private final static SimpleDateFormat sdfTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private final static SimpleDateFormat sdfHMS = new SimpleDateFormat("HH:mm:ss");

    private final static SimpleDateFormat sdfSymbol = new SimpleDateFormat("yyyy年MM月dd日");

    /**
     * parse yyyy-MM-dd by yyyy年MM月dd日
     * @param formatDate
     * @return
     */
    public static String parseSymbolDateToDate(String formatDate) {
        Date date = null;
        try {
            date = (Date) sdfSymbol.parseObject(formatDate);
        } catch (Exception e) {
            // TODO 加日志打印
            System.out.println("format date is error. date = " + formatDate);
            //log.error("format date is error. date = " + formatDate);
        }
        if(date != null){
            return getDay(date);
        }
        return null;
    }

    /**
     * 获取YYYY-MM-DD格式
     *
     * @return
     */
    public static String getDay() {
        return getDay(new Date());
    }

    /**
     * 获取YYYY-MM-DD格式
     * @param date
     * @return
     */
    public static String getDay(Date date) {
        return sdfDay.format(date);
    }

    /**
     * 获取YYYY-MM-DD HH:mm:ss格式
     *
     * @return
     */
    public static String getTime() {
        return getTime(new Date());
    }

    /**
     * 获取YYYY-MM-DD HH:mm:ss格式
     *
     * @return
     */
    public static String getTime(Date date) {
        return sdfTime.format(date);
    }

    /**
     * 获取HH:mm:ss格式
     * @return
     */
    public static String getHMS() {
        return getHMS(new Date());
    }

    /**
     * 获取HH:mm:ss格式
     * @param date
     * @return
     */
    public static String getHMS(Date date) {
        return sdfHMS.format(date);
    }

    /**
     * 获取当天的开始时间
     * @return
     */
    public static Date getDayStartTime() {
        return getDayStartTime(new Date());
    }

    /**
     * 获取当天的结束时间
     * @return
     */
    public static Date getDayEndTime() {
        return getDayEndTime(new Date());
    }

    /**
     * 获取某个日期的开始时间
     * @param date
     * @return
     */
    public static Date getDayStartTime(Date date) {
        Calendar calendar = getCalendar(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * 获取某个日期的结束时间
     * @param date
     * @return
     */
    public static Date getDayEndTime(Date date) {
        Calendar calendar = getCalendar(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime();
    }
    /**
     * parse date by yyy-MM-dd
     * @param formatDate
     * @return
     */
    public static Date parseStringDayToDate(String formatDate) {
        Date date;
        try {
            date = (Date) sdfDay.parseObject(formatDate);
        } catch (Exception e) {
            date = new Date();
            // TODO 加日志打印
            System.out.println("format date is error. date = " + formatDate);
            //log.error("format date is error. date = " + formatDate);
        }
        return date;
    }


    /**
     * 获取指定时间的前（后）index天的日期
     * @param date
     * @param index
     * @return
     */
    public static Date getAnyDay(Date date, int index) {
        Calendar calendar = getCalendar(date);
        getAnyDay(calendar, index);
        return calendar.getTime();
    }

    /**
     * 获取calendar
     * @param date
     * @return
     */
    private static Calendar getCalendar(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }

    private static void getAnyDay(Calendar calendar, int index) {
        calendar.add(Calendar.DATE, index);
    }

    /**
     * parse date by yyyy-MM-dd HH:mm:ss
     * @param formatDate
     * @return
     */
    public static Date parseStringTimeToDate(String formatDate) {
        Date date;
        try {
            date = DateUtil.parse(formatDate);
        } catch (Exception e) {
            date = null;
            // TODO 加日志打印
            System.out.println("format date is error. date =" + formatDate);
            //log.error("format date is error. date =" + formatDate);
        }
        return date;
    }

    private static Date fillDay(Calendar calendar, int type, int index) {
        calendar.add(type, index);
        return calendar.getTime();
    }

    /**
     * 获取指定日期的当月开始日期
     * @param date
     * @return
     */
    private static Date getMonthStart(Date date) {
        Calendar calendar = getCalendar(date);
        return fillDay(calendar, Calendar.DATE, (1 - calendar.get(Calendar.DAY_OF_MONTH)));
    }

    /**
     * 获取指定日期的当月结束日期
     * @param date
     * @return
     */
    private static Date getMonthEnd(Date date) {
        Calendar calendar = getCalendar(date);
        calendar.add(Calendar.MONTH, 1);
        return fillDay(calendar, Calendar.DATE, (-calendar.get(Calendar.DAY_OF_MONTH)));
    }



    /**
     * 获取当月所有天
     * @return
     */
    public static List<String> getDayListOfMonth(int day) {
        List<String> list = new ArrayList<String>();
        Calendar aCalendar = Calendar.getInstance(Locale.CHINA);
        int year = aCalendar.get(Calendar.YEAR);//年份
        int month = aCalendar.get(Calendar.MONTH) + 1;//月份
        for (int i = 1; i <= day; i++) {
            String aDate = String.valueOf(year)+"-"+month+"-"+i;
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date d1 = format.parse(aDate);
                list.add(format.format(d1));
            } catch (ParseException e) {
                e.printStackTrace();
            }


        }
        return list;
    }

    public static void main(String[] args) {

        String date = parseSymbolDateToDate("2021年1月2日");
        System.out.println(date);

        System.out.println(formDate1(new Date()));

        System.out.println(getHMS());
    }

    public static String formDate1(Date value) {
        String newValue = "";
        try {
            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            if (value == null) {
                return "";
            }
            Calendar ca = Calendar.getInstance();
            ca.setTime(value);
            ca.add(Calendar.HOUR_OF_DAY, 12);
            newValue = f.format(ca.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newValue;
    }

    /**
     *
     * @param year 年份
     * @param month 月
     * @return 返回当年当月的最大天数
     */

    public int getMaxDayByYearMonth(int year, int month) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DATE, 1);
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month - 1);
        return calendar.getActualMaximum(Calendar.DATE);
    }
    public static String YYYY_MM_DD = "yyyy-MM-dd";

    public static String getDate()
    {
        return dateTimeNow(YYYY_MM_DD);
    }

    public static final String dateTimeNow(final String format)
    {
        return parseDateToStr(format, new Date());
    }
    public static final String parseDateToStr(final String format, final Date date)
    {
        return new SimpleDateFormat(format).format(date);
    }

    public static int compareTime(Date dateOne, Date dateTwo){
        Calendar calendarStart = Calendar.getInstance();
        Calendar calendarEnd = Calendar.getInstance();

        try {
            calendarStart.setTime(dateOne);
            calendarEnd.setTime(dateTwo);
        } catch (Exception e) {
//            e.printStackTrace();
            return 100;
        }

        int result = calendarStart.compareTo(calendarEnd);
        if(result > 0){
            result = 1;
        }else if(result < 0){
            result = -1;
        }else{
            result = 0 ;
        }
        return result ;
    }

    public static int compareTime(String dateOne, String dateTwo , String dateFormatType){

        DateFormat df = new SimpleDateFormat(dateFormatType);
        Calendar calendarStart = Calendar.getInstance();
        Calendar calendarEnd = Calendar.getInstance();

        try {
            calendarStart.setTime(df.parse(dateOne));
            calendarEnd.setTime(df.parse(dateTwo));
        } catch (ParseException e) {
            e.printStackTrace();
            return 100;
        }

        int result = calendarStart.compareTo(calendarEnd);
        if(result > 0){
            result = 1;
        }else if(result < 0){
            result = -1;
        }else{
            result = 0 ;
        }
        return result ;
    }

    /**
     * data转LocalDateTime
     * @param date
     * @return
     */
    public static LocalDateTime date2LocalDateTime(Date date){
        if (ObjectUtil.isEmpty( date )){
            return null;
        }
        ZoneId zoneId = ZoneId.systemDefault();
        return LocalDateTime.ofInstant( date.toInstant(),zoneId );
    }

}
